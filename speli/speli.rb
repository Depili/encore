#!/usr/bin/env ruby

require 'portmidi'
require 'highline'
require_relative '../utils/midi.rb'
require_relative '../utils/amulet.rb'

midi_ids = [Portmidi.input_devices.last.device_id, Portmidi.output_devices.last.device_id]

puts "Using MIDI device id: #{midi_ids[0]} as input"
puts

puts "Using MIDI device id: #{midi_ids[1]} as output"
puts


device_id = midi_ids[0]

#input =  Portmidi::Input.new(device_id.to_i)

keys = EncoreMidi.keys(:lc).select {|k| k[0] < 4}
puts "Game will use #{keys.count} keys..."


mutex = Mutex.new
game_state = {
  buffer: [],
  score: 0,
  game_over: false,
  last_key: [0,0],
  delay: 3
}
encore = EncoreMidi.new(midi_ids[0], midi_ids[1])

encore.reset_leds(:lc, true)
sleep(game_state[:delay])
encore.reset_leds(:lc, false)


gamethread = Thread.new do
  loop do
    while(!game_state[:game_over] && game_state[:buffer].count < 100) do
      if game_state[:start]
        sleep(0.1)
        next
      end
      # Get a new key
      key = keys.sample
      while key == game_state[:last_key]
        key = keys.sample()
      end
      mutex.synchronize   do

        puts key.inspect
        game_state[:last_key] = key

        encore.set_led(*key, true)

        game_state[:buffer] << key
      end
      if game_state[:score] == 0
        game_state[:start] = true
        puts "Waiting for start..."
        next
      end

      sleep(game_state[:delay])
      encore.set_led(*key, false)

    end
  end
end

listen_thread = Thread.new do
  encore.listen do |msg|
    puts "#{msg[:chan]}: #{msg[:type]} #{msg.key?(:note) ? msg[:note] : msg[:control]}"
    if msg[:type] == :note_on
      key = [msg[:chan], msg[:note]]
      mutex.synchronize   do
        if game_state[:start]
          if key == game_state[:buffer].first
            puts "Starting game..."
            game_state[:score] = 1
            game_state[:buffer].shift
            game_state[:start] = false
            encore.set_led(*key, false)
          end
        elsif key == game_state[:buffer].shift
          encore.set_led(*key, false)
          game_state[:score] += 1
          game_state[:delay] *= 0.95
          if game_state[:delay] < 0.3
            game_state[:delay] == 0.3
          end
          encore.show_score(game_state[:score])
        elsif game_state[:score] > 0
          encore.show_score(game_state[:score])
          encore.reset_leds(:lc, true)
          sleep 1
          encore.reset_leds(:lc, false)
          encore.show_score(game_state[:score])

          sleep 5
          exit
          game_state[:game_over] = true
          game_state[:start] = false
          game_state[:score] = 0
          game_state[:delay] = 4
          game_state[:game_over] = false
          game_state[:buffer] = []
        end
      end
    end
  end
end

gamethread.join