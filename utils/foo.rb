require 'chunky_png'

def draw_hhl_logo(a, amulet = nil)
  lines = {}

  y_off = (240 - 107) / 2

  amulet.draw_fill_rect 0,0,320,240,"F"

  def draw_pixel(x, y, pattern, length)
    if length > 16
      end_y = y + length
      @f.puts "#{0xD9.chr}%04X%04X%04X%04X#{pattern}" % [x, y, x, end_y]
      @long += 1
    else
      @f.puts "#{0xDC.chr}%04X%04X#{pattern}%02X" % [x, y, length]
      @short += 1
    end
  end

  @f = File.open "foo.txt", "wb"
  @long = @short = 0

  cat = ChunkyPNG::Image.from_file(a)
  x = 0
  y = 0
  count = 0 # counter for consecutive black pixels
  # a.set_word 0xff, 0x0050
  while y < 107 do
    lines[y] = []
    while x < 320 do
      if cat[x,y] == 0xFFFFFFFF
        count += 1
      end
      if (cat[x,y] != 0xFFFFFFFF && count > 0)
        lines[y] << [x - count, count]
        # draw_pixel x - count + 1, y + ((240 - 107) / 2), "0", count
        count = 0
      end
      x += 1
    end
    if count != 0
      lines[y] << [x - count, count]
      # draw_pixel x - count + 1, y + ((240 - 107) / 2), "0", count
      count = 0
    end
    y += 1
    x = 0
  end

  combined_lines = []

  lines.each_key do |y|
    lines[y].each do |segment|
      i = 0
      while lines.key?(y+i+1) do
        if lines[y+i+1].include? segment
          lines[y+i+1].delete segment
          i += 1
        else
          break
        end
      end
      if i > 0
        combined_lines << [segment[0], y + y_off, segment[1], i+1]
        if amulet
          amulet.draw_fill_rect(segment[0], y + y_off, segment[1], i+1, "0", "1")
          sleep(0.015)
        end
        lines[y].delete segment
      end
    end
  end

  normal_lines = []
  pixels = []


  lines.each_key do |y|
    lines[y].each do |segment|
      x = segment[0]
      length = segment[1]
      if length > 15
        end_x = x + length
        normal_lines << "%04X%04X%04X%04X01" % [x, y + y_off, end_x, y + y_off]
        if amulet
          amulet.draw_line(x, y + y_off, end_x, y + y_off, "0", "1")
          sleep(0.015)
        end

        @long += 1
      else
        pixels << "%04X%04X0%1X" % [x, y + y_off, length ]

        if amulet
          amulet.draw_pixel(x, y + y_off, "0", length.to_s(16).upcase)
          sleep(0.015)
        end


        @short += 1
      end
    end
  end

  @f.puts "const char rectangles[#{combined_lines.count}][19] = {"
  combined_lines.each do |l|
    @f.puts "\t\"%04X%04X%04X%04X01\", " % l
  end
  @f.puts "};"
  @f.puts ""

  @f.puts "const char lines[#{@long}][19] = {"
  normal_lines.each do |l|
    @f.puts "\t\"#{l}\","
  end
  @f.puts "};"
  @f.puts ""

  @f.puts "const char pixels[#{@short}][10] = {"
  pixels.each do |l|
    @f.puts "\t\"#{l}\","
  end
  @f.puts "};"
  @f.puts ""


  @f.close

  puts "Rectangles: #{combined_lines.count}"
  puts "Long lines: #{@long}, short lines: #{@short}"
  puts "Total: #{@long + @short + combined_lines.count}"
end


def draw_cat(a)
  cat = ChunkyPNG::Image.from_file('cat.png')
  x = 0
  y = 0
  count = 0 # counter for consecutive black pixels
  # a.set_word 0xff, 0x0050
  a.draw_fill_rect 0,0,320,240,"F"
  a.draw_fill_rect_2 0,0,320,240,"F"
  while y < 240 do
    while x < 320 do
      if cat[x,y] != 0xffffffff
        count += 1
      end
      if (count == 15) || (cat[x,y] == 0xffffffff && count > 0)
        a.draw_pixel x - count + 1, y, "0", count.to_s(16).upcase
        a.draw_pixel_2 x - count + 1, y, "0", count.to_s(16).upcase
        sleep(0.015)
        count = 0
      end
      x += 1
    end
    if count != 0
      a.draw_pixel x - count + 1, y, "0", count.to_s(16).upcase
      a.draw_pixel_2 x - count + 1, y, "0", count.to_s(16).upcase
      sleep(0.015)
      count = 0
    end
    y += 1
    x = 0
  end
end