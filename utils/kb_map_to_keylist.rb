#!/usr/bin/env ruby

file = ARGV.first

File.open(file, "r") do |f|
  f.each_line do |l|
    next if l.strip[0] == '#'
    kb_num, keycode, led_addr, led_bit = *l.split(" ")
    # puts "#{kb_num}, #{keycode}, #{led_addr}, #{led_bit}"
    puts "[#{kb_num}, #{keycode}],"
  end
end