require 'rubyserial'
require 'colorize'
require 'logger'


class Amulet
  def initialize(serial, output_tty = false)
    @sp = Serial.new serial, 115200
    @output_tty = output_tty
    @log = Logger.new "amulet_log_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.log"
    @log.level = :info

    @ack = true
    listen
  end

  def ack?
    @ack ? true : false
  end

  def set_byte(addr, val)
    @ack = false
    @sp.write(0xD5.chr + "%02X%02X" % [addr, val])
    @log.info "-> Set byte variable Addr: %02X Value: %02X" % [addr, val]
  end

  def set_word(addr, val)
    @ack = false
    @sp.write(0xD6.chr + "%02X%04X" % [addr, val])
    @log.info "-> Set word variable Addr: %02X Value: %04X" % [addr, val]
  end

  # TODO: test and verify used charset?
  # Because of this, we have come up with a way to send upper
  # ASCII characters to the Amulet using the UART protocol by
  # using the DLE(0x10) escape character. When the Amulet sees
  # a DLE as part of a text string, it will add 0x60 to the next
  # character to come up with the upper ASCII character. Using
  # one DLE escape allows you to send characters in the range of
  # 0x80-0xDF. It is legal to concatenate DLE escapes for
  # characters greater than 0xDF. So using two DLE escapes in
  # a row allows you to send characters in the range of 0xE0-0xFF.
  # TODO:
  # 0x02 == font style escape
  # following byte:
  # 0x01 - bold
  # 0x02 - italic
  # 0x04 - strikethrough
  # 0x08 - underline (does not work?)
  # 0x80 - plain
  #
  # Whitelist log...
  def set_string_8bit(addr, str)
    payload = str.encode("ASCII-8BIT").bytes.collect do |b|
      if b < 0x80
        b.chr
      elsif b < 0xe0
        [0x10.chr, (b - 0x60).chr]
      else
        [0x10.chr, 0x10.chr, (b - 0x60 - 0x60).chr]
      end
    end
    @ack = false
    @sp.write(0xD7.chr + "%02X#{payload.flatten.join("")}#{0x00.chr}" % [addr])
    @log.info "-> Set string variable Addr: %02X Value: " % [addr]
  end

  # Send just 7bit ascii
  def set_string(addr, str)
    payload = str.encode("ASCII")
    @ack = false
    @sp.write(0xD7.chr + "%02X#{payload}#{0x00.chr}" % [addr])
    @log.info "-> Set string variable Addr: %02X Value: " % [addr]
  end

  def draw_line(x1, y1, x2, y2, pattern = "0", width = "4")
    @ack = false
    @sp.write(0xD9.chr + "%04X%04X%04X%04X#{pattern}#{width}" % [x1, y1, x2, y2] )
    @log.info "-> Draw line"
  end

  def draw_rect(x1, y1, x2, y2, pattern = "0", width = "4")
    @ack = false
    @sp.write(0xDA.chr + "%04X%04X%04X%04X#{pattern}#{width}" % [x1, y1, x2, y2] )
    @log.info "-> Draw rectangle"
  end

  def draw_fill_rect(x1, y1, x2, y2, pattern = "0", width = "4")
    @ack = false
    @sp.write(0xDB.chr + "%04X%04X%04X%04X#{pattern}#{width}" % [x1, y1, x2, y2] )
    @log.info "-> Draw filled rectangle"
  end

  def draw_fill_rect_2(x1, y1, x2, y2, pattern = "0", width = "4")
    @ack = false
    @sp.write(0xBB.chr + "%04X%04X%04X%04X#{pattern}#{width}" % [x1, y1, x2, y2] )
    @log.info "-> Draw filled rectangle"
  end


  def draw_pixel(x, y, pattern = "0", width = "4")
    @ack = false
    @sp.write(0xDC.chr + "%04X%04X#{pattern}#{width}" % [x, y] )
    @log.info "-> Draw pixel"
  end

  def draw_pixel_2(x, y, pattern = "0", width = "4")
    @sp.write(0xBC.chr + "%04X%04X#{pattern}#{width}" % [x, y] )
    @log.info "-> Draw pixel"
  end


  def get_byte(addr)
    @ack = false
    @sp.write(0xD0.chr + "%02X" % [addr])
    @log.info "-> Get byte variable Addr: %02X" % [addr]
  end

  def get_word(addr)
    @ack = false
    @sp.write(0xD1.chr + "%02X" % [addr])
    @log.info "-> Get word variable Addr: %02X" % [addr]
  end

  def get_string(addr)
    @ack = false
    @sp.write(0xD2.chr + "%02X" % [addr])
    @log.info "-> Get string variable Addr: %02X Value: %02X" % [addr]
  end

  def self.commands
    {
      0xA0 => [5, 'PAGE', 'Jump to page'],
      0xD0 => [3, 'GBV', 'Get byte variable'],
      0xD1 => [3, 'GWV', 'Get word variable'],
      0xD2 => [3, 'GSV', 'Get string variable'],
      0xD3 => [3, 'GLV', 'Get label variable'],
      0xD4 => [3, 'GRPC', 'Get RPC buffer'],
      0xD5 => [5, 'SBV', 'Set byte variable'],
      0xD6 => [7, 'SWV', 'Set word variable'],
      0xD7 => [nil, 'SSV', 'Set string variable'],
      0xD8 => [3, 'RPC', 'Invoke RPC'],
      0xD9 => [24, 'LINE', 'Draw line'],
      0xDA => [24, 'RECT', 'Draw rectangle'],
      0xDB => [24, 'FRECT', 'Draw filled rectangle'],
      0xDC => [11, 'PIXEL', 'Draw pixel'],
      0xDD => [3, 'GBVA', 'Get byte variable array'],
      0xDE => [3, 'GWVA', 'Get word variable array'],
      0xDF => [nil, 'SBVA', 'Set byte variable array'],
      0xE0 => [5, 'GBVR', 'Get byte variable reply'],
      0xE1 => [7, 'GWVR', 'Get word variable reply'],
      0xE2 => [nil, 'GSVR', 'Get string variable reply'],
      0xE3 => [nil, 'GLVR', 'Get label variable reply'],
      0xE4 => [nil, 'GRPCR', 'Get RPC buffer reply'],
      0xE5 => [5, 'SBVR', 'Set byte variable reply'],
      0xE6 => [7, 'SWVR', 'Set word variable reply'],
      0xE7 => [nil, 'SSVR', 'Set string variable reply'],
      0xE8 => [3, 'RPCR', 'Invoke RPC reply'],
      0xE9 => [24, 'LINER', 'Draw line reply'],
      0xEA => [24, 'RECTR', 'Draw rectangle'],
      0xEB => [24, 'FRECTR', 'Draw filled rectangle reply'],
      0xEC => ['PIXELR', 'Draw pixel reply'],
      0xED => [nil, 'GBVAR', 'Get byte variable array reply'],
      0xEE => [nil, 'GWVAR', 'Get word variable array reply'],
      0xEF => [5, 'SBVAR', 'Set byte variable array reply'],
      0xF0 => [1, 'ACK', 'Acknowledgment'],
      0xF1 => [1, 'NACK', 'Negative acknowledgment'],
      0xF2 => [nil, 'SWVA', 'Set word variable array'],
      0xF3 => [5, 'SWVAR', 'Set word variable array reply'],
      0xF4 => [3, 'GCV', 'Get color variable'],
      0xF5 => [11, 'GCVR', 'Get color variable reply'],
      0xF6 => [11, 'SCV', 'Set color variable'],
      0xF7 => [11, 'SCVR', 'Set color variable reply']
    }
  end

  def self.address?(c)
    has_addr = [
      0xD0,
      0xD1,
      0xD2,
      0xD3,
      0xD4,
      0xD5,
      0xD6,
      0xD7,
      0xD8,
      0xDD,
      0xDE,
      0xDF,
      0xE0,
      0xE1,
      0xE2,
      0xE3,
      0xE4,
      0xE5,
      0xE6,
      0xE7,
      0xE8,
      0xED,
      0xEE,
      0xEF,
      0xF2,
      0xF3,
      0xF4,
      0xF5,
      0xF6,
      0xF7
    ]
    has_addr.include? c
  end

  def self.byte?(cmd)
    byte_cmds = [
      0xD0,
      0xD5,
      0xE0,
      0xE5
    ]
    byte_cmds.include? cmd
  end

  def self.word?(cmd)
    word_cmds = [
      0xD1,
      0xD6,
      0xE1,
      0xE6
    ]
    word_cmds.include? cmd
  end

  def self.string?(cmd)
    string_cmds = [
      0xD2,
      0xD7,
      0xE2,
      0xE7
    ]
    string_cmds.include? cmd
  end

  private

  def num_or_letter?(char)
    if char =~ /[a-zA-Z0-9]/
      true
    elsif char =~ Regexp.union(" ", ".", "?", "-", "+", "/", ",", "(", ")", ":")
      true
    end
  end

  def tty_puts(output)
    if @output_tty
      puts output
    end
  end

  def listen
    Thread.new do
      cmd = nil
      cmd_len = nil
      bytes = 0
      string = nil
      decode = nil
      addr = nil
      @log.info "Listening thread active"
      loop do
        c = @sp.getbyte
        unless c.nil?
          if cmd.nil?
            if Amulet.commands.key? c
              cmd = c
              cmd_len = Amulet.commands[c][0]
              bytes = 1
              string = "%02X - " %c
              decode = "#{Amulet.commands[c][2]} "
              @ack = true if cmd == 0xF0
            else
              @log.warn "Unknown command byte: %02X" % c
              tty_puts(("Unknown command byte: %02X" % c).red)
            end
          else
            string += "%02X" % c
            if Amulet.address? cmd
              if bytes == 2
                addr = c.chr
              elsif bytes == 3
                addr += c.chr
                decode += "Addr: #{addr} "
                if Amulet.byte? cmd
                  case addr
                  when 'A0'
                    decode += "[Tab 1 enable] "
                  when 'A1'
                    decode += "[Tab 2 enable] "
                  when 'A2'
                    decode += "[Tab 3 enable] "
                  when 'A3'
                    decode += "[Tab 4 enable] "
                  when 'A4'
                    decode += "[Tab 5 enable] "
                  when 'A5'
                    decode += "[Tab 6 enable] "
                  when 'A6'
                    decode += "[Tab 7 enable] "
                  when 'A7'
                    decode += "[Tab 8 enable] "
                  when "FF"
                    decode += "[touch event?] "
                  end
                elsif Amulet.word? cmd
                  case addr
                  when 'FF'
                    decode += "[page change?] "
                  end
                elsif Amulet.string? cmd
                  case addr
                  when 'A0'
                    decode += "[Tab 1] "
                  when 'A1'
                    decode += "[Tab 2] "
                  when 'A2'
                    decode += "[Tab 3] "
                  when 'A3'
                    decode += "[Tab 4] "
                  when 'A4'
                    decode += "[Tab 5] "
                  when 'A5'
                    decode += "[Tab 6] "
                  when 'A6'
                    decode += "[Tab 7] "
                  when 'A7'
                    decode += "[Tab 8] "
                  when 'C5'
                    decode += "[Heading] "
                  when '9F'
                    decode += "[Amulet version?] "
                  end
                end
                decode += "Value: "
              else
                # TODO: Deal with 0x10 bytes..
                if num_or_letter? c.chr
                  decode += c.chr
                else
                  decode += "."
                end
              end
            else
              if num_or_letter? c.chr
                decode += c.chr
              else
                decode += "."
              end
            end
          end
          if (!cmd_len.nil? && bytes == cmd_len) || (c == 0x00 && !decode.empty?)
            # End of command
            cmd = nil
            cmd_len = nil
            tty_puts "RAW: #{string}".yellow
            @log.debug "RAW: #{string}"
            tty_puts "DECODE: #{decode}"
            @log.info "DECODE: #{decode}"
          end
          bytes += 1
        else
          sleep 0.05
        end
      end
    end
  end
end

