#!/usr/bin/env ruby

file = ARGV.first

File.open(file, "r") do |f|
  f.each_line do |l|
    next if l.strip[0] == '#'
    kb_num, keycode, led_addr, led_bit = *l.split(" ")
    # puts "#{kb_num}, #{keycode}, #{led_addr}, #{led_bit}"

    print "\tif (kb_num == #{kb_num} && keycode == #{keycode}) {"
    print " addr = #{led_addr};"
    print " led_bit = #{led_bit};"
    print "}"
    puts ""
  end
end