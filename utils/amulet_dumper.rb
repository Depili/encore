#!/usr/bin/env ruby

require 'rubyserial'
require 'colorize'
require_relative 'amulet.rb'

log = File.open "capture_log_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.log", "w"
puts "Log file open: #{log.path}"

serial = Serial.new ARGV.first, 115200

cmd = nil
cmd_len = nil
bytes = 0
string = nil
decode = nil
addr = nil

loop do
  c = serial.getbyte
  unless c.nil?
    if cmd.nil?
      if @amulet_commands.key? c
        cmd = c
        cmd_len = @amulet_commands[c][0]
        bytes = 1
        string = "%02X - " %c
        decode = "#{@amulet_commands[c][2]} "
      else
        log.puts "Unknown command byte: %02X" % c
        puts(("Unknown command byte: %02X" % c).red)
      end
    else
      string += "%02X" % c
      if amulet_address? cmd
        if bytes == 2
          addr = c.chr
        elsif bytes == 3
          addr += c.chr
          decode += "Addr: #{addr} "
          if amulet_byte? cmd
            case addr
            when 'A0'
              decode += "[Tab 1 enable] "
            when 'A1'
              decode += "[Tab 2 enable] "
            when 'A2'
              decode += "[Tab 3 enable] "
            when 'A3'
              decode += "[Tab 4 enable] "
            when 'A4'
              decode += "[Tab 5 enable] "
            when 'A5'
              decode += "[Tab 6 enable] "
            when 'A6'
              decode += "[Tab 7 enable] "
            when 'A7'
              decode += "[Tab 8 enable] "
            when "FF"
              decode += "[touch event?] "
            end
          elsif amulet_word? cmd
            case addr
            when 'FF'
              decode += "[page change?] "
            end
          elsif amulet_string? cmd
            case addr
            when 'A0'
              decode += "[Tab 1] "
            when 'A1'
              decode += "[Tab 2] "
            when 'A2'
              decode += "[Tab 3] "
            when 'A3'
              decode += "[Tab 4] "
            when 'A4'
              decode += "[Tab 5] "
            when 'A5'
              decode += "[Tab 6] "
            when 'A6'
              decode += "[Tab 7] "
            when 'A7'
              decode += "[Tab 8] "
            when 'C5'
              decode += "[Heading] "
            when '9F'
              decode += "[Amulet version?] "
            end
          end
          decode += "Value: "
        else
          if num_or_letter? c.chr
            decode += c.chr
          else
            decode += "."
          end
        end
      else
        if num_or_letter? c.chr
          decode += c.chr
        else
          decode += "."
        end
      end
    end
    if (!cmd_len.nil? && bytes == cmd_len) || c == 0x00
      # End of command
      cmd = nil
      cmd_len = nil
      puts "RAW: #{string}".yellow
      log.puts "RAW: #{string}"
      puts "DECODE: #{decode}"
      log.puts "DECODE: #{decode}"
    end
    bytes += 1
  end
end