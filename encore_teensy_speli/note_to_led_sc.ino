// Transform a led address byte to led map index
uint8_t led_addr_to_index(uint8_t addr) {
  for (uint8_t i = 0; 0 < LEDS_N; i++) {
    if (LED_ADDR[i] == addr)
      return i;
  }
  return 0;
}

#ifdef ENCORE_SC

void note_to_led(uint8_t kb_num, uint8_t keycode, bool led_state) {
  uint8_t addr = 0xFF;
  uint8_t led_bit = 0xFF;

  // Start of generated code

  if (kb_num == 4 && keycode == 0x50) {
    addr = 0x30;
    led_bit = 7;
  }

  if (kb_num == 4 && keycode == 0x51) {
    addr = 0x30;
    led_bit = 6;
  }

  if (kb_num == 4 && keycode == 0x52) {
    addr = 0x30;
    led_bit = 5;
  }

  if (kb_num == 4 && keycode == 0x58) {
    addr = 0x30;
    led_bit = 4;
  }

  if (kb_num == 4 && keycode == 0x59) {
    addr = 0x30;
    led_bit = 3;
  }

  if (kb_num == 4 && keycode == 0x5A) {
    addr = 0x30;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x00) {
    addr = 0x00;
    led_bit = 7;
  }

  if (kb_num == 1 && keycode == 0x01) {
    addr = 0x00;
    led_bit = 6;
  }

  if (kb_num == 1 && keycode == 0x02) {
    addr = 0x00;
    led_bit = 5;
  }

  if (kb_num == 1 && keycode == 0x03) {
    addr = 0x00;
    led_bit = 4;
  }

  if (kb_num == 1 && keycode == 0x04) {
    addr = 0x00;
    led_bit = 3;
  }

  if (kb_num == 1 && keycode == 0x05) {
    addr = 0x00;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x06) {
    addr = 0x00;
    led_bit = 1;
  }

  if (kb_num == 1 && keycode == 0x07) {
    addr = 0x00;
    led_bit = 0;
  }

  if (kb_num == 1 && keycode == 0x08) {
    addr = 0x00;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x09) {
    addr = 0x00;
    led_bit = 23;
  }

  if (kb_num == 1 && keycode == 0x0A) {
    addr = 0x00;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x0B) {
    addr = 0x00;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x0C) {
    addr = 0x00;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x10) {
    addr = 0x00;
    led_bit = 16;
  }

  if (kb_num == 1 && keycode == 0x11) {
    addr = 0x00;
    led_bit = 31;
  }

  if (kb_num == 1 && keycode == 0x12) {
    addr = 0x01;
    led_bit = 7;
  }

  if (kb_num == 1 && keycode == 0x13) {
    addr = 0x01;
    led_bit = 6;
  }

  if (kb_num == 1 && keycode == 0x14) {
    addr = 0x01;
    led_bit = 5;
  }

  if (kb_num == 1 && keycode == 0x15) {
    addr = 0x01;
    led_bit = 4;
  }

  if (kb_num == 1 && keycode == 0x16) {
    addr = 0x01;
    led_bit = 3;
  }

  if (kb_num == 1 && keycode == 0x17) {
    addr = 0x01;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x18) {
    addr = 0x01;
    led_bit = 1;
  }

  if (kb_num == 1 && keycode == 0x1A) {
    addr = 0x01;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x1B) {
    addr = 0x01;
    led_bit = 23;
  }

  if (kb_num == 1 && keycode == 0x1C) {
    addr = 0x01;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x1D) {
    addr = 0x01;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x1E) {
    addr = 0x01;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x1F) {
    addr = 0x01;
    led_bit = 19;
  }

  if (kb_num == 1 && keycode == 0x20) {
    addr = 0x01;
    led_bit = 18;
  }

  if (kb_num == 1 && keycode == 0x21) {
    addr = 0x01;
    led_bit = 17;
  }

  if (kb_num == 1 && keycode == 0x23) {
    addr = 0x01;
    led_bit = 31;
  }

  if (kb_num == 1 && keycode == 0x24) {
    addr = 0x02;
    led_bit = 7;
  }

  if (kb_num == 1 && keycode == 0x25) {
    addr = 0x02;
    led_bit = 6;
  }

  if (kb_num == 1 && keycode == 0x26) {
    addr = 0x02;
    led_bit = 5;
  }

  if (kb_num == 1 && keycode == 0x27) {
    addr = 0x02;
    led_bit = 4;
  }

  if (kb_num == 1 && keycode == 0x29) {
    addr = 0x02;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x2A) {
    addr = 0x02;
    led_bit = 1;
  }

  if (kb_num == 1 && keycode == 0x2B) {
    addr = 0x02;
    led_bit = 0;
  }

  if (kb_num == 1 && keycode == 0x2C) {
    addr = 0x02;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x2D) {
    addr = 0x02;
    led_bit = 23;
  }

  if (kb_num == 1 && keycode == 0x2E) {
    addr = 0x02;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x2F) {
    addr = 0x02;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x30) {
    addr = 0x02;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x31) {
    addr = 0x02;
    led_bit = 19;
  }

  if (kb_num == 1 && keycode == 0x32) {
    addr = 0x02;
    led_bit = 18;
  }

  if (kb_num == 1 && keycode == 0x34) {
    addr = 0x02;
    led_bit = 16;
  }

  if (kb_num == 1 && keycode == 0x35) {
    addr = 0x02;
    led_bit = 31;
  }

  if (kb_num == 1 && keycode == 0x36) {
    addr = 0x03;
    led_bit = 7;
  }

  if (kb_num == 1 && keycode == 0x37) {
    addr = 0x03;
    led_bit = 6;
  }

  if (kb_num == 1 && keycode == 0x38) {
    addr = 0x03;
    led_bit = 5;
  }

  if (kb_num == 1 && keycode == 0x39) {
    addr = 0x03;
    led_bit = 4;
  }

  if (kb_num == 1 && keycode == 0x3A) {
    addr = 0x03;
    led_bit = 3;
  }

  if (kb_num == 1 && keycode == 0x3B) {
    addr = 0x03;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x3C) {
    addr = 0x03;
    led_bit = 1;
  }

  if (kb_num == 1 && keycode == 0x3E) {
    addr = 0x03;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x40) {
    addr = 0x03;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x41) {
    addr = 0x03;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x42) {
    addr = 0x03;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x43) {
    addr = 0x03;
    led_bit = 19;
  }

  if (kb_num == 1 && keycode == 0x44) {
    addr = 0x03;
    led_bit = 18;
  }

  if (kb_num == 1 && keycode == 0x45) {
    addr = 0x03;
    led_bit = 17;
  }

  if (kb_num == 1 && keycode == 0x46) {
    addr = 0x03;
    led_bit = 16;
  }

  if (kb_num == 1 && keycode == 0x4F) {
    addr = 0x04;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x51) {
    addr = 0x04;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x52) {
    addr = 0x04;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x53) {
    addr = 0x04;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x54) {
    addr = 0x04;
    led_bit = 19;
  }

  if (kb_num == 1 && keycode == 0x55) {
    addr = 0x04;
    led_bit = 18;
  }

  if (kb_num == 1 && keycode == 0x56) {
    addr = 0x04;
    led_bit = 17;
  }

  if (kb_num == 1 && keycode == 0x57) {
    addr = 0x04;
    led_bit = 16;
  }

  if (kb_num == 1 && keycode == 0x59) {
    addr = 0x05;
    led_bit = 7;
  }

  if (kb_num == 1 && keycode == 0x5A) {
    addr = 0x05;
    led_bit = 6;
  }

  if (kb_num == 1 && keycode == 0x5B) {
    addr = 0x05;
    led_bit = 5;
  }

  if (kb_num == 1 && keycode == 0x5C) {
    addr = 0x05;
    led_bit = 4;
  }

  if (kb_num == 1 && keycode == 0x5D) {
    addr = 0x05;
    led_bit = 3;
  }

  if (kb_num == 1 && keycode == 0x5E) {
    addr = 0x05;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x61) {
    addr = 0x05;
    led_bit = 15;
  }

  if (kb_num == 1 && keycode == 0x62) {
    addr = 0x05;
    led_bit = 23;
  }

  if (kb_num == 1 && keycode == 0x63) {
    addr = 0x05;
    led_bit = 22;
  }

  if (kb_num == 1 && keycode == 0x64) {
    addr = 0x05;
    led_bit = 21;
  }

  if (kb_num == 1 && keycode == 0x65) {
    addr = 0x05;
    led_bit = 20;
  }

  if (kb_num == 1 && keycode == 0x66) {
    addr = 0x05;
    led_bit = 19;
  }

  if (kb_num == 1 && keycode == 0x67) {
    addr = 0x05;
    led_bit = 18;
  }

  if (kb_num == 1 && keycode == 0x68) {
    addr = 0x05;
    led_bit = 17;
  }

  if (kb_num == 1 && keycode == 0x69) {
    addr = 0x05;
    led_bit = 16;
  }

  if (kb_num == 1 && keycode == 0x6F) {
    addr = 0x06;
    led_bit = 2;
  }

  if (kb_num == 1 && keycode == 0x70) {
    addr = 0x06;
    led_bit = 1;
  }

  if (kb_num == 1 && keycode == 0x71) {
    addr = 0x06;
    led_bit = 0;
  }

  if (kb_num == 5 && keycode == 0x00) {
    addr = 0x00;
    led_bit = 7;
  }

  if (kb_num == 5 && keycode == 0x01) {
    addr = 0x00;
    led_bit = 6;
  }

  if (kb_num == 5 && keycode == 0x02) {
    addr = 0x00;
    led_bit = 5;
  }

  if (kb_num == 5 && keycode == 0x03) {
    addr = 0x00;
    led_bit = 4;
  }

  if (kb_num == 5 && keycode == 0x04) {
    addr = 0x00;
    led_bit = 3;
  }

  if (kb_num == 5 && keycode == 0x05) {
    addr = 0x00;
    led_bit = 2;
  }

  if (kb_num == 5 && keycode == 0x06) {
    addr = 0x00;
    led_bit = 1;
  }

  if (kb_num == 5 && keycode == 0x07) {
    addr = 0x00;
    led_bit = 0;
  }

  if (kb_num == 5 && keycode == 0x36) {
    addr = 0x03;
    led_bit = 7;
  }

  if (kb_num == 5 && keycode == 0x37) {
    addr = 0x03;
    led_bit = 6;
  }

  if (kb_num == 5 && keycode == 0x38) {
    addr = 0x03;
    led_bit = 5;
  }

  if (kb_num == 5 && keycode == 0x39) {
    addr = 0x03;
    led_bit = 4;
  }

  if (kb_num == 5 && keycode == 0x3A) {
    addr = 0x03;
    led_bit = 3;
  }

  if (kb_num == 5 && keycode == 0x3B) {
    addr = 0x03;
    led_bit = 2;
  }

  // End of generated code

  if (addr == 0xFF) {
    return;
  }

  uint32_t led_value = 1 << led_bit;
  uint32_t row = led_map[led_addr_to_index(addr)];
  if (led_state) {
    row |= led_value;
  } else {
    row &= ~led_value;
  }
  led_map[led_addr_to_index(addr)] = row;
}

#else

// Encore LC mapping

void note_to_led(uint8_t kb_num, uint8_t keycode, bool led_state) {
  uint8_t addr = 0xFF;
  uint8_t led_bit = 0xFF;

  // Start of generated code

  if (kb_num == 2 && keycode == 0x00) { addr = 0x07; led_bit = 7;}
  if (kb_num == 2 && keycode == 0x01) { addr = 0x07; led_bit = 6;}
  if (kb_num == 2 && keycode == 0x02) { addr = 0x07; led_bit = 5;}
  if (kb_num == 2 && keycode == 0x03) { addr = 0x07; led_bit = 4;}
  if (kb_num == 2 && keycode == 0x04) { addr = 0x07; led_bit = 3;}
  if (kb_num == 2 && keycode == 0x05) { addr = 0x07; led_bit = 2;}
  if (kb_num == 2 && keycode == 0x06) { addr = 0x07; led_bit = 1;}
  if (kb_num == 2 && keycode == 0x07) { addr = 0x07; led_bit = 0;}
  if (kb_num == 2 && keycode == 0x08) { addr = 0x07; led_bit = 15;}
  if (kb_num == 2 && keycode == 0x09) { addr = 0x07; led_bit = 23;}
  if (kb_num == 2 && keycode == 0x0A) { addr = 0x07; led_bit = 22;}
  if (kb_num == 2 && keycode == 0x0B) { addr = 0x07; led_bit = 21;}
  if (kb_num == 2 && keycode == 0x0C) { addr = 0x07; led_bit = 20;}
  if (kb_num == 2 && keycode == 0x0D) { addr = 0x07; led_bit = 19;}
  if (kb_num == 2 && keycode == 0x0E) { addr = 0x07; led_bit = 18;}
  if (kb_num == 2 && keycode == 0x0F) { addr = 0x07; led_bit = 17;}
  if (kb_num == 2 && keycode == 0x10) { addr = 0x07; led_bit = 16;}
  if (kb_num == 2 && keycode == 0x11) { addr = 0x07; led_bit = 31;}
  if (kb_num == 3 && keycode == 0x00) { addr = 0x10; led_bit = 7;}
  if (kb_num == 3 && keycode == 0x01) { addr = 0x10; led_bit = 6;}
  if (kb_num == 3 && keycode == 0x02) { addr = 0x10; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x03) { addr = 0x10; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x04) { addr = 0x10; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x05) { addr = 0x10; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x06) { addr = 0x10; led_bit = 1;}
  if (kb_num == 3 && keycode == 0x07) { addr = 0x10; led_bit = 0;}
  if (kb_num == 3 && keycode == 0x08) { addr = 0x10; led_bit = 15;}
  if (kb_num == 3 && keycode == 0x09) { addr = 0x10; led_bit = 23;}
  if (kb_num == 3 && keycode == 0x0A) { addr = 0x10; led_bit = 22;}
  if (kb_num == 3 && keycode == 0x0B) { addr = 0x10; led_bit = 21;}
  if (kb_num == 3 && keycode == 0x0C) { addr = 0x10; led_bit = 20;}
  if (kb_num == 3 && keycode == 0x0D) { addr = 0x10; led_bit = 19;}
  if (kb_num == 3 && keycode == 0x0E) { addr = 0x10; led_bit = 18;}
  if (kb_num == 3 && keycode == 0x0F) { addr = 0x10; led_bit = 17;}
  if (kb_num == 3 && keycode == 0x10) { addr = 0x10; led_bit = 16;}
  if (kb_num == 5 && keycode == 0x00) { addr = 0x08; led_bit = 7;}
  if (kb_num == 5 && keycode == 0x01) { addr = 0x08; led_bit = 6;}
  if (kb_num == 5 && keycode == 0x02) { addr = 0x08; led_bit = 5;}
  if (kb_num == 5 && keycode == 0x03) { addr = 0x08; led_bit = 4;}
  if (kb_num == 5 && keycode == 0x04) { addr = 0x08; led_bit = 3;}
  if (kb_num == 5 && keycode == 0x05) { addr = 0x08; led_bit = 2;}
  if (kb_num == 5 && keycode == 0x06) { addr = 0x08; led_bit = 1;}
  if (kb_num == 5 && keycode == 0x07) { addr = 0x08; led_bit = 0;}
  if (kb_num == 5 && keycode == 0x08) { addr = 0x08; led_bit = 15;}
  if (kb_num == 5 && keycode == 0x09) { addr = 0x08; led_bit = 23;}
  if (kb_num == 5 && keycode == 0x0A) { addr = 0x08; led_bit = 22;}
  if (kb_num == 5 && keycode == 0x0B) { addr = 0x08; led_bit = 21;}
  if (kb_num == 5 && keycode == 0x0C) { addr = 0x08; led_bit = 20;}
  if (kb_num == 5 && keycode == 0x0D) { addr = 0x08; led_bit = 19;}
  if (kb_num == 5 && keycode == 0x0E) { addr = 0x08; led_bit = 18;}
  if (kb_num == 5 && keycode == 0x0F) { addr = 0x08; led_bit = 19;}
  if (kb_num == 5 && keycode == 0x10) { addr = 0x08; led_bit = 18;}
  if (kb_num == 5 && keycode == 0x11) { addr = 0x08; led_bit = 17;}
  if (kb_num == 5 && keycode == 0x12) { addr = 0x08; led_bit = 16;}
  if (kb_num == 5 && keycode == 0x13) { addr = 0x08; led_bit = 31;}
  if (kb_num == 6 && keycode == 0x00) { addr = 0x19; led_bit = 7;}
  if (kb_num == 6 && keycode == 0x01) { addr = 0x19; led_bit = 6;}
  if (kb_num == 6 && keycode == 0x02) { addr = 0x19; led_bit = 5;}
  if (kb_num == 6 && keycode == 0x03) { addr = 0x19; led_bit = 4;}
  if (kb_num == 6 && keycode == 0x04) { addr = 0x19; led_bit = 3;}
  if (kb_num == 6 && keycode == 0x05) { addr = 0x19; led_bit = 2;}
  if (kb_num == 6 && keycode == 0x06) { addr = 0x19; led_bit = 1;}
  if (kb_num == 6 && keycode == 0x07) { addr = 0x19; led_bit = 0;}
  if (kb_num == 6 && keycode == 0x08) { addr = 0x19; led_bit = 15;}
  if (kb_num == 6 && keycode == 0x09) { addr = 0x19; led_bit = 23;}
  if (kb_num == 6 && keycode == 0x0A) { addr = 0x19; led_bit = 22;}
  if (kb_num == 6 && keycode == 0x0B) { addr = 0x19; led_bit = 21;}
  if (kb_num == 2 && keycode == 0x12) { addr = 0x09; led_bit = 7;}
  if (kb_num == 2 && keycode == 0x13) { addr = 0x09; led_bit = 6;}
  if (kb_num == 2 && keycode == 0x14) { addr = 0x09; led_bit = 5;}
  if (kb_num == 2 && keycode == 0x15) { addr = 0x09; led_bit = 4;}
  if (kb_num == 2 && keycode == 0x16) { addr = 0x09; led_bit = 3;}
  if (kb_num == 2 && keycode == 0x17) { addr = 0x09; led_bit = 2;}
  if (kb_num == 2 && keycode == 0x18) { addr = 0x09; led_bit = 1;}
  if (kb_num == 2 && keycode == 0x19) { addr = 0x09; led_bit = 0;}
  if (kb_num == 2 && keycode == 0x1A) { addr = 0x09; led_bit = 15;}
  if (kb_num == 2 && keycode == 0x1B) { addr = 0x09; led_bit = 23;}
  if (kb_num == 2 && keycode == 0x1C) { addr = 0x09; led_bit = 22;}
  if (kb_num == 2 && keycode == 0x1D) { addr = 0x09; led_bit = 21;}
  if (kb_num == 2 && keycode == 0x1E) { addr = 0x09; led_bit = 20;}
  if (kb_num == 2 && keycode == 0x1F) { addr = 0x09; led_bit = 19;}
  if (kb_num == 2 && keycode == 0x20) { addr = 0x09; led_bit = 18;}
  if (kb_num == 2 && keycode == 0x21) { addr = 0x09; led_bit = 17;}
  if (kb_num == 2 && keycode == 0x22) { addr = 0x09; led_bit = 16;}
  if (kb_num == 2 && keycode == 0x23) { addr = 0x09; led_bit = 31;}
  if (kb_num == 3 && keycode == 0x12) { addr = 0x11; led_bit = 7;}
  if (kb_num == 3 && keycode == 0x13) { addr = 0x11; led_bit = 6;}
  if (kb_num == 3 && keycode == 0x14) { addr = 0x11; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x15) { addr = 0x11; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x16) { addr = 0x11; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x17) { addr = 0x11; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x18) { addr = 0x11; led_bit = 1;}
  if (kb_num == 3 && keycode == 0x19) { addr = 0x11; led_bit = 0;}
  if (kb_num == 3 && keycode == 0x1A) { addr = 0x11; led_bit = 15;}
  if (kb_num == 3 && keycode == 0x1B) { addr = 0x11; led_bit = 23;}
  if (kb_num == 3 && keycode == 0x1C) { addr = 0x11; led_bit = 22;}
  if (kb_num == 3 && keycode == 0x1D) { addr = 0x11; led_bit = 21;}
  if (kb_num == 3 && keycode == 0x1E) { addr = 0x11; led_bit = 20;}
  if (kb_num == 3 && keycode == 0x1F) { addr = 0x11; led_bit = 19;}
  if (kb_num == 3 && keycode == 0x20) { addr = 0x11; led_bit = 18;}
  if (kb_num == 3 && keycode == 0x21) { addr = 0x11; led_bit = 17;}
  if (kb_num == 2 && keycode == 0x36) { addr = 0x23; led_bit = 7;}
  if (kb_num == 2 && keycode == 0x37) { addr = 0x23; led_bit = 6;}
  if (kb_num == 2 && keycode == 0x38) { addr = 0x23; led_bit = 5;}
  if (kb_num == 2 && keycode == 0x39) { addr = 0x23; led_bit = 4;}
  if (kb_num == 2 && keycode == 0x3A) { addr = 0x23; led_bit = 3;}
  if (kb_num == 2 && keycode == 0x3B) { addr = 0x23; led_bit = 2;}
  if (kb_num == 2 && keycode == 0x3C) { addr = 0x23; led_bit = 1;}
  if (kb_num == 2 && keycode == 0x3D) { addr = 0x23; led_bit = 0;}
  if (kb_num == 2 && keycode == 0x3E) { addr = 0x23; led_bit = 15;}
  if (kb_num == 2 && keycode == 0x3F) { addr = 0x23; led_bit = 23;}
  if (kb_num == 2 && keycode == 0x40) { addr = 0x23; led_bit = 22;}
  if (kb_num == 2 && keycode == 0x41) { addr = 0x23; led_bit = 21;}
  if (kb_num == 2 && keycode == 0x42) { addr = 0x23; led_bit = 20;}
  if (kb_num == 2 && keycode == 0x43) { addr = 0x23; led_bit = 19;}
  if (kb_num == 2 && keycode == 0x44) { addr = 0x23; led_bit = 18;}
  if (kb_num == 2 && keycode == 0x45) { addr = 0x23; led_bit = 17;}
  if (kb_num == 2 && keycode == 0x46) { addr = 0x23; led_bit = 16;}
  if (kb_num == 5 && keycode == 0x36) { addr = 0x27; led_bit = 7;}
  if (kb_num == 5 && keycode == 0x37) { addr = 0x27; led_bit = 6;}
  if (kb_num == 5 && keycode == 0x38) { addr = 0x27; led_bit = 5;}
  if (kb_num == 5 && keycode == 0x39) { addr = 0x27; led_bit = 4;}
  if (kb_num == 5 && keycode == 0x3A) { addr = 0x27; led_bit = 3;}
  if (kb_num == 5 && keycode == 0x3B) { addr = 0x27; led_bit = 2;}
  if (kb_num == 5 && keycode == 0x3C) { addr = 0x27; led_bit = 1;}
  if (kb_num == 5 && keycode == 0x3D) { addr = 0x27; led_bit = 0;}
  if (kb_num == 5 && keycode == 0x3E) { addr = 0x27; led_bit = 15;}
  if (kb_num == 5 && keycode == 0x3F) { addr = 0x27; led_bit = 23;}
  if (kb_num == 5 && keycode == 0x40) { addr = 0x27; led_bit = 22;}
  if (kb_num == 5 && keycode == 0x41) { addr = 0x27; led_bit = 21;}
  if (kb_num == 5 && keycode == 0x42) { addr = 0x27; led_bit = 20;}
  if (kb_num == 5 && keycode == 0x43) { addr = 0x27; led_bit = 19;}
  if (kb_num == 5 && keycode == 0x44) { addr = 0x27; led_bit = 18;}
  if (kb_num == 5 && keycode == 0x45) { addr = 0x27; led_bit = 17;}
  if (kb_num == 5 && keycode == 0x46) { addr = 0x27; led_bit = 16;}
  if (kb_num == 2 && keycode == 0x59) { addr = 0x25; led_bit = 7;}
  if (kb_num == 2 && keycode == 0x5A) { addr = 0x25; led_bit = 6;}
  if (kb_num == 2 && keycode == 0x5B) { addr = 0x25; led_bit = 5;}
  if (kb_num == 2 && keycode == 0x5C) { addr = 0x25; led_bit = 4;}
  if (kb_num == 2 && keycode == 0x5D) { addr = 0x25; led_bit = 3;}
  if (kb_num == 2 && keycode == 0x5E) { addr = 0x25; led_bit = 2;}
  if (kb_num == 2 && keycode == 0x5F) { addr = 0x25; led_bit = 1;}
  if (kb_num == 2 && keycode == 0x60) { addr = 0x25; led_bit = 0;}
  if (kb_num == 2 && keycode == 0x61) { addr = 0x25; led_bit = 15;}
  if (kb_num == 2 && keycode == 0x62) { addr = 0x25; led_bit = 23;}
  if (kb_num == 2 && keycode == 0x63) { addr = 0x25; led_bit = 22;}
  if (kb_num == 2 && keycode == 0x64) { addr = 0x25; led_bit = 21;}
  if (kb_num == 2 && keycode == 0x65) { addr = 0x25; led_bit = 20;}
  if (kb_num == 2 && keycode == 0x66) { addr = 0x25; led_bit = 19;}
  if (kb_num == 2 && keycode == 0x67) { addr = 0x25; led_bit = 18;}
  if (kb_num == 2 && keycode == 0x68) { addr = 0x25; led_bit = 17;}
  if (kb_num == 5 && keycode == 0x59) { addr = 0x28; led_bit = 7;}
  if (kb_num == 5 && keycode == 0x5A) { addr = 0x28; led_bit = 6;}
  if (kb_num == 5 && keycode == 0x5B) { addr = 0x28; led_bit = 5;}
  if (kb_num == 5 && keycode == 0x5C) { addr = 0x28; led_bit = 4;}
  if (kb_num == 5 && keycode == 0x5D) { addr = 0x28; led_bit = 3;}
  if (kb_num == 5 && keycode == 0x5E) { addr = 0x28; led_bit = 2;}
  if (kb_num == 5 && keycode == 0x5F) { addr = 0x28; led_bit = 1;}
  if (kb_num == 5 && keycode == 0x60) { addr = 0x28; led_bit = 0;}
  if (kb_num == 5 && keycode == 0x61) { addr = 0x28; led_bit = 15;}
  if (kb_num == 5 && keycode == 0x62) { addr = 0x28; led_bit = 23;}
  if (kb_num == 5 && keycode == 0x63) { addr = 0x28; led_bit = 22;}
  if (kb_num == 5 && keycode == 0x64) { addr = 0x28; led_bit = 21;}
  if (kb_num == 5 && keycode == 0x65) { addr = 0x28; led_bit = 20;}
  if (kb_num == 5 && keycode == 0x66) { addr = 0x28; led_bit = 19;}
  if (kb_num == 5 && keycode == 0x67) { addr = 0x28; led_bit = 18;}
  if (kb_num == 5 && keycode == 0x68) { addr = 0x28; led_bit = 17;}
  if (kb_num == 4 && keycode == 0x50) { addr = 0x30; led_bit = 7;}
  if (kb_num == 4 && keycode == 0x51) { addr = 0x30; led_bit = 6;}
  if (kb_num == 4 && keycode == 0x52) { addr = 0x30; led_bit = 5;}
  if (kb_num == 4 && keycode == 0x58) { addr = 0x30; led_bit = 4;}
  if (kb_num == 4 && keycode == 0x59) { addr = 0x30; led_bit = 3;}
  if (kb_num == 4 && keycode == 0x5A) { addr = 0x30; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x36) { addr = 0x13; led_bit = 7;}
  if (kb_num == 3 && keycode == 0x47) { addr = 0x03; led_bit = 7;}
  if (kb_num == 3 && keycode == 0x38) { addr = 0x13; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x39) { addr = 0x13; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x3A) { addr = 0x13; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x3B) { addr = 0x13; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x3C) { addr = 0x13; led_bit = 1;}
  if (kb_num == 3 && keycode == 0x3D) { addr = 0x13; led_bit = 0;}
  if (kb_num == 3 && keycode == 0x3E) { addr = 0x13; led_bit = 15;}
  if (kb_num == 3 && keycode == 0x3F) { addr = 0x13; led_bit = 23;}
  if (kb_num == 3 && keycode == 0x40) { addr = 0x13; led_bit = 22;}
  if (kb_num == 3 && keycode == 0x41) { addr = 0x13; led_bit = 21;}
  if (kb_num == 3 && keycode == 0x42) { addr = 0x13; led_bit = 20;}
  if (kb_num == 3 && keycode == 0x43) { addr = 0x13; led_bit = 19;}
  if (kb_num == 3 && keycode == 0x49) { addr = 0x03; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x4A) { addr = 0x03; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x4B) { addr = 0x03; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x4C) { addr = 0x03; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x4D) { addr = 0x03; led_bit = 1;}
  if (kb_num == 3 && keycode == 0x4E) { addr = 0x03; led_bit = 0;}
  if (kb_num == 3 && keycode == 0x4F) { addr = 0x03; led_bit = 15;}
  if (kb_num == 3 && keycode == 0x50) { addr = 0x03; led_bit = 23;}
  if (kb_num == 3 && keycode == 0x51) { addr = 0x03; led_bit = 22;}
  if (kb_num == 3 && keycode == 0x52) { addr = 0x03; led_bit = 21;}
  if (kb_num == 3 && keycode == 0x53) { addr = 0x03; led_bit = 20;}
  if (kb_num == 3 && keycode == 0x54) { addr = 0x03; led_bit = 19;}
  if (kb_num == 3 && keycode == 0x59) { addr = 0x15; led_bit = 7;}
  if (kb_num == 3 && keycode == 0x5A) { addr = 0x15; led_bit = 6;}
  if (kb_num == 3 && keycode == 0x5B) { addr = 0x15; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x5C) { addr = 0x15; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x5D) { addr = 0x15; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x5E) { addr = 0x15; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x5F) { addr = 0x15; led_bit = 1;}
  if (kb_num == 3 && keycode == 0x60) { addr = 0x15; led_bit = 0;}
  if (kb_num == 3 && keycode == 0x61) { addr = 0x15; led_bit = 15;}
  if (kb_num == 3 && keycode == 0x62) { addr = 0x15; led_bit = 23;}
  if (kb_num == 3 && keycode == 0x63) { addr = 0x15; led_bit = 22;}
  if (kb_num == 3 && keycode == 0x64) { addr = 0x15; led_bit = 21;}
  if (kb_num == 3 && keycode == 0x65) { addr = 0x15; led_bit = 20;}
  if (kb_num == 3 && keycode == 0x66) { addr = 0x15; led_bit = 19;}
  if (kb_num == 3 && keycode == 0x68) { addr = 0x15; led_bit = 17;}
  if (kb_num == 6 && keycode == 0x59) { addr = 0x06; led_bit = 7;}
  if (kb_num == 6 && keycode == 0x5A) { addr = 0x06; led_bit = 6;}
  if (kb_num == 6 && keycode == 0x5B) { addr = 0x06; led_bit = 5;}
  if (kb_num == 6 && keycode == 0x5C) { addr = 0x06; led_bit = 4;}
  if (kb_num == 6 && keycode == 0x5D) { addr = 0x06; led_bit = 3;}
  if (kb_num == 6 && keycode == 0x5E) { addr = 0x06; led_bit = 2;}
  if (kb_num == 6 && keycode == 0x5F) { addr = 0x06; led_bit = 1;}
  if (kb_num == 6 && keycode == 0x60) { addr = 0x06; led_bit = 0;}
  if (kb_num == 6 && keycode == 0x61) { addr = 0x06; led_bit = 15;}
  if (kb_num == 6 && keycode == 0x62) { addr = 0x06; led_bit = 23;}
  if (kb_num == 6 && keycode == 0x63) { addr = 0x06; led_bit = 22;}
  if (kb_num == 6 && keycode == 0x64) { addr = 0x06; led_bit = 21;}
  if (kb_num == 6 && keycode == 0x65) { addr = 0x06; led_bit = 20;}
  if (kb_num == 6 && keycode == 0x66) { addr = 0x06; led_bit = 19;}
  if (kb_num == 6 && keycode == 0x68) { addr = 0x06; led_bit = 17;}
  if (kb_num == 3 && keycode == 0x6B) { addr = 0x16; led_bit = 6;}
  if (kb_num == 3 && keycode == 0x6C) { addr = 0x16; led_bit = 5;}
  if (kb_num == 3 && keycode == 0x6D) { addr = 0x16; led_bit = 4;}
  if (kb_num == 3 && keycode == 0x6E) { addr = 0x16; led_bit = 3;}
  if (kb_num == 3 && keycode == 0x6F) { addr = 0x16; led_bit = 2;}
  if (kb_num == 3 && keycode == 0x70) { addr = 0x16; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x00) { addr = 0x00; led_bit = 7;}
  if (kb_num == 1 && keycode == 0x01) { addr = 0x00; led_bit = 6;}
  if (kb_num == 1 && keycode == 0x02) { addr = 0x00; led_bit = 5;}
  if (kb_num == 1 && keycode == 0x03) { addr = 0x00; led_bit = 4;}
  if (kb_num == 1 && keycode == 0x04) { addr = 0x00; led_bit = 3;}
  if (kb_num == 1 && keycode == 0x05) { addr = 0x00; led_bit = 2;}
  if (kb_num == 1 && keycode == 0x06) { addr = 0x00; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x07) { addr = 0x00; led_bit = 0;}
  if (kb_num == 1 && keycode == 0x14) { addr = 0x01; led_bit = 5;}
  if (kb_num == 1 && keycode == 0x15) { addr = 0x01; led_bit = 4;}
  if (kb_num == 1 && keycode == 0x16) { addr = 0x01; led_bit = 3;}
  if (kb_num == 1 && keycode == 0x17) { addr = 0x01; led_bit = 2;}
  if (kb_num == 1 && keycode == 0x18) { addr = 0x01; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x19) { addr = 0x01; led_bit = 0;}
  if (kb_num == 1 && keycode == 0x1A) { addr = 0x01; led_bit = 15;}
  if (kb_num == 1 && keycode == 0x1B) { addr = 0x01; led_bit = 23;}
  if (kb_num == 1 && keycode == 0x1C) { addr = 0x01; led_bit = 22;}
  if (kb_num == 1 && keycode == 0x1D) { addr = 0x01; led_bit = 21;}
  if (kb_num == 1 && keycode == 0x1E) { addr = 0x01; led_bit = 20;}
  if (kb_num == 1 && keycode == 0x1F) { addr = 0x01; led_bit = 19;}
  if (kb_num == 1 && keycode == 0x20) { addr = 0x01; led_bit = 18;}
  if (kb_num == 1 && keycode == 0x21) { addr = 0x01; led_bit = 17;}
  if (kb_num == 1 && keycode == 0x22) { addr = 0x01; led_bit = 16;}
  if (kb_num == 1 && keycode == 0x23) { addr = 0x01; led_bit = 31;}
  if (kb_num == 1 && keycode == 0x0E) { addr = 0x00; led_bit = 18;}
  if (kb_num == 1 && keycode == 0x0F) { addr = 0x00; led_bit = 17;}
  if (kb_num == 1 && keycode == 0x10) { addr = 0x00; led_bit = 16;}
  if (kb_num == 1 && keycode == 0x11) { addr = 0x00; led_bit = 31;}
  if (kb_num == 1 && keycode == 0x26) { addr = 0x02; led_bit = 5;}
  if (kb_num == 1 && keycode == 0x27) { addr = 0x02; led_bit = 4;}
  if (kb_num == 1 && keycode == 0x28) { addr = 0x02; led_bit = 3;}
  if (kb_num == 1 && keycode == 0x29) { addr = 0x02; led_bit = 2;}
  if (kb_num == 1 && keycode == 0x2A) { addr = 0x02; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x2B) { addr = 0x02; led_bit = 0;}
  if (kb_num == 1 && keycode == 0x2C) { addr = 0x02; led_bit = 15;}
  if (kb_num == 1 && keycode == 0x2D) { addr = 0x02; led_bit = 23;}
  if (kb_num == 1 && keycode == 0x2E) { addr = 0x02; led_bit = 22;}
  if (kb_num == 1 && keycode == 0x2F) { addr = 0x02; led_bit = 21;}
  if (kb_num == 1 && keycode == 0x30) { addr = 0x02; led_bit = 20;}
  if (kb_num == 1 && keycode == 0x31) { addr = 0x02; led_bit = 19;}
  if (kb_num == 1 && keycode == 0x32) { addr = 0x02; led_bit = 18;}
  if (kb_num == 1 && keycode == 0x33) { addr = 0x02; led_bit = 17;}
  if (kb_num == 1 && keycode == 0x34) { addr = 0x02; led_bit = 16;}
  if (kb_num == 1 && keycode == 0x35) { addr = 0x02; led_bit = 31;}
  if (kb_num == 1 && keycode == 0x4A) { addr = 0x04; led_bit = 4;}
  if (kb_num == 1 && keycode == 0x4B) { addr = 0x04; led_bit = 3;}
  if (kb_num == 1 && keycode == 0x4C) { addr = 0x04; led_bit = 2;}
  if (kb_num == 1 && keycode == 0x4D) { addr = 0x04; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x4E) { addr = 0x04; led_bit = 0;}
  if (kb_num == 1 && keycode == 0x4F) { addr = 0x04; led_bit = 15;}
  if (kb_num == 1 && keycode == 0x50) { addr = 0x04; led_bit = 23;}
  if (kb_num == 1 && keycode == 0x51) { addr = 0x04; led_bit = 22;}
  if (kb_num == 1 && keycode == 0x52) { addr = 0x04; led_bit = 21;}
  if (kb_num == 1 && keycode == 0x53) { addr = 0x04; led_bit = 20;}
  if (kb_num == 1 && keycode == 0x54) { addr = 0x04; led_bit = 19;}
  if (kb_num == 1 && keycode == 0x47) { addr = 0x04; led_bit = 7;}
  if (kb_num == 1 && keycode == 0x48) { addr = 0x04; led_bit = 6;}
  if (kb_num == 1 && keycode == 0x59) { addr = 0x05; led_bit = 7;}
  if (kb_num == 1 && keycode == 0x5A) { addr = 0x05; led_bit = 6;}
  if (kb_num == 1 && keycode == 0x5C) { addr = 0x05; led_bit = 4;}
  if (kb_num == 1 && keycode == 0x5D) { addr = 0x05; led_bit = 3;}
  if (kb_num == 1 && keycode == 0x5E) { addr = 0x05; led_bit = 2;}
  if (kb_num == 1 && keycode == 0x5F) { addr = 0x05; led_bit = 1;}
  if (kb_num == 1 && keycode == 0x60) { addr = 0x05; led_bit = 0;}
  if (kb_num == 1 && keycode == 0x61) { addr = 0x05; led_bit = 15;}
  if (kb_num == 1 && keycode == 0x62) { addr = 0x05; led_bit = 23;}
  if (kb_num == 1 && keycode == 0x63) { addr = 0x05; led_bit = 22;}
  if (kb_num == 1 && keycode == 0x64) { addr = 0x05; led_bit = 21;}
  if (kb_num == 1 && keycode == 0x65) { addr = 0x05; led_bit = 20;}
  if (kb_num == 1 && keycode == 0x66) { addr = 0x05; led_bit = 19;}

  // End of generated code

  if (addr == 0xFF) {
    return;
  }

  uint32_t led_value = 1 << led_bit;
  uint32_t row = led_map[led_addr_to_index(addr)];
  if (led_state) {
    row |= led_value;
  } else {
    row &= ~led_value;
  }
  led_map[led_addr_to_index(addr)] = row;
}

#endif
