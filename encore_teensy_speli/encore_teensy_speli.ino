/*
   Teensy 3.5 program for using the Barco Folsom Encore LC and SC keyboards

   Plug the Teensy to the ribbon cable that is normally between the carrier board
   and the keyboard.

   PINS:

  TODO: add pin table
  TODO: Route pins 14 and 15 in and re-init on their changes?
*/

#include "SPI.h"
#include "encore.h"

uint8_t scoreLeds[15][2] {
  {5,   0x0E},
  {5,   0x0D},
  {5,   0x0C},
  {5,   0x0B},
  {5,   0x0A},
  {5,   0x09},
  {5,   0x08},
  {5,   0x07},
  {5,   0x06},
  {5,   0x05},
  {5,   0x04},
  {5,   0x03},
  {5,   0x02},
  {5,   0x01},
  {5,   0x00},
};

uint8_t hiScoreLeds[15][2] {
  {5,   0x44},
  {5,   0x43},
  {5,   0x42},
  {5,   0x41},
  {5,   0x40},
  {5,   0x3F},
  {5,   0x3E},
  {5,   0x3D},
  {5,   0x3C},
  {5,   0x3B},
  {5,   0x3A},
  {5,   0x39},
  {5,   0x38},
  {5,   0x37},
  {5,   0x36},
};

uint8_t lcKeys[218][2] {
  {2, 0x00},
  {2, 0x01},
  {2, 0x02},
  {2, 0x03},
  {2, 0x04},
  {2, 0x05},
  {2, 0x06},
  {2, 0x07},
  {2, 0x08},
  {2, 0x09},
  {2, 0x0A},
  {2, 0x0B},
  {2, 0x0C},
  {2, 0x0D},
  {2, 0x0E},
  {2, 0x0F},
  {2, 0x10},
  {2, 0x11},
  {3, 0x00},
  {3, 0x01},
  {3, 0x02},
  {3, 0x03},
  {3, 0x04},
  {3, 0x05},
  {3, 0x06},
  {3, 0x07},
  {3, 0x08},
  {3, 0x09},
  {3, 0x0A},
  {3, 0x0B},
  {3, 0x0C},
  {3, 0x0D},
  {3, 0x0E},
  {3, 0x0F},
  {3, 0x10},
  {2, 0x12},
  {2, 0x13},
  {2, 0x14},
  {2, 0x15},
  {2, 0x16},
  {2, 0x17},
  {2, 0x18},
  {2, 0x19},
  {2, 0x1A},
  {2, 0x1B},
  {2, 0x1C},
  {2, 0x1D},
  {2, 0x1E},
  {2, 0x1F},
  {2, 0x20},
  {2, 0x21},
  {2, 0x22},
  {2, 0x23},
  {3, 0x12},
  {3, 0x13},
  {3, 0x14},
  {3, 0x15},
  {3, 0x16},
  {3, 0x17},
  {3, 0x18},
  {3, 0x19},
  {3, 0x1A},
  {3, 0x1B},
  {3, 0x1C},
  {3, 0x1D},
  {3, 0x1E},
  {3, 0x1F},
  {3, 0x20},
  {3, 0x21},
  {2, 0x36},
  {2, 0x37},
  {2, 0x38},
  {2, 0x39},
  {2, 0x3A},
  {2, 0x3B},
  {2, 0x3C},
  {2, 0x3D},
  {2, 0x3E},
  {2, 0x3F},
  {2, 0x40},
  {2, 0x41},
  {2, 0x42},
  {2, 0x43},
  {2, 0x44},
  {2, 0x45},
  {2, 0x46},
  {2, 0x59},
  {2, 0x5A},
  {2, 0x5B},
  {2, 0x5C},
  {2, 0x5D},
  {2, 0x5E},
  {2, 0x5F},
  {2, 0x60},
  {2, 0x61},
  {2, 0x62},
  {2, 0x63},
  {2, 0x64},
  {2, 0x65},
  {2, 0x66},
  {2, 0x67},
  {2, 0x68},
  {3, 0x36},
  {3, 0x47},
  {3, 0x38},
  {3, 0x39},
  {3, 0x3A},
  {3, 0x3B},
  {3, 0x3C},
  {3, 0x3D},
  {3, 0x3E},
  {3, 0x3F},
  {3, 0x40},
  {3, 0x41},
  {3, 0x42},
  {3, 0x43},
  {3, 0x49},
  {3, 0x4A},
  {3, 0x4B},
  {3, 0x4C},
  {3, 0x4D},
  {3, 0x4E},
  {3, 0x4F},
  {3, 0x50},
  {3, 0x51},
  {3, 0x52},
  {3, 0x53},
  {3, 0x54},
  {3, 0x59},
  {3, 0x5A},
  {3, 0x5B},
  {3, 0x5C},
  {3, 0x5D},
  {3, 0x5E},
  {3, 0x5F},
  {3, 0x60},
  {3, 0x61},
  {3, 0x62},
  {3, 0x63},
  {3, 0x64},
  {3, 0x65},
  {3, 0x66},
  {3, 0x68},
  {3, 0x6B},
  {3, 0x6C},
  {3, 0x6D},
  {3, 0x6E},
  {3, 0x6F},
  {3, 0x70},
  {1, 0x00},
  {1, 0x01},
  {1, 0x02},
  {1, 0x03},
  {1, 0x04},
  {1, 0x05},
  {1, 0x06},
  {1, 0x07},
  {1, 0x14},
  {1, 0x15},
  {1, 0x16},
  {1, 0x17},
  {1, 0x18},
  {1, 0x19},
  {1, 0x1A},
  {1, 0x1B},
  {1, 0x1C},
  {1, 0x1D},
  {1, 0x1E},
  {1, 0x1F},
  {1, 0x20},
  {1, 0x21},
  {1, 0x22},
  {1, 0x23},
  {1, 0x0E},
  {1, 0x0F},
  {1, 0x10},
  {1, 0x11},
  {1, 0x26},
  {1, 0x27},
  {1, 0x28},
  {1, 0x29},
  {1, 0x2A},
  {1, 0x2B},
  {1, 0x2C},
  {1, 0x2D},
  {1, 0x2E},
  {1, 0x2F},
  {1, 0x30},
  {1, 0x31},
  {1, 0x32},
  {1, 0x33},
  {1, 0x34},
  {1, 0x35},
  {1, 0x4A},
  {1, 0x4B},
  {1, 0x4C},
  {1, 0x4D},
  {1, 0x4E},
  {1, 0x4F},
  {1, 0x50},
  {1, 0x51},
  {1, 0x52},
  {1, 0x53},
  {1, 0x54},
  {1, 0x47},
  {1, 0x48},
  {1, 0x59},
  {1, 0x5A},
  {1, 0x5C},
  {1, 0x5D},
  {1, 0x5E},
  {1, 0x5F},
  {1, 0x60},
  {1, 0x61},
  {1, 0x62},
  {1, 0x63},
  {1, 0x64},
  {1, 0x65},
};


// ADC input via SPI MODE3 slave and interrupts
volatile uint32_t spi_map;
volatile uint32_t spi_irq;
uint8_t adc_message[14];
uint16_t adc_data[7];
volatile uint8_t adc_data_index = 0;
volatile bool process_adc_data = false;
// Decoded ADC values
uint16_t tbar, joy_x, joy_y, joy_z;
int16_t encoders[6] = {0, 0, 0, 0, 0, 0};

// LED control via SPI MODE3 master
elapsedMicros led_interval;   // Update interval for leds
uint32_t led_map[LEDS_N];     // Led bitmap

// Initialization flags
volatile bool ready_to_init = false;
volatile bool init_done = false;

typedef struct keyPress {
  uint8_t kb;
  uint8_t key;
} keyEvent;

keyEvent queue[100];
uint8_t queue_read = 0;
uint8_t queue_write = 0;
uint16_t currentScore = 0;
uint8_t gameState = 0;
uint16_t gameDelay = 5000;
uint16_t gameScore = 0;
uint16_t gameHiScore = 0;

unsigned long gameTimeout;

HardwareSerial *keyboard_serials[] = {&KB_1_UART, &KB_2_UART, &KB_3_UART, &KB_4_UART};


void setup() {

  // USB serial
  Serial.begin(115200);

  // General input/output setup for pins
  setupPins();

  // Keyboards
  KB_1_UART.begin(KB_UART_MODE);
  KB_2_UART.begin(KB_UART_MODE);
  KB_3_UART.begin(KB_UART_MODE);
  KB_4_UART.begin(KB_UART_MODE);

  // SPI0 in SPI_MODE3 as a slave
  spi0SlaveSetup();

  // Led control
  initLedSPI();
  initLedMap();

  // Displays
  LCD1_UART.begin(LCD_UART_MODE);
  LCD2_UART.begin(LCD_UART_MODE);

  // Enable line buffers
  digitalWrite(CE, LOW);

  // Send the led bitmap and reset the counter
  sendLedMap();
  led_interval = 0;

  // Initialize the usb-midi interface
  init_midi();

  attachInterrupt(PIN14_IN, detectReset, FALLING);
  for (int i = 0; i < 100; i++) {
    queue[i].kb = 0;
    queue[i].key = 0;
  }
}

void loop() {

  if (init_done) {
    // Run the main processing loop
    mainLoop();
  } else {
    if (ready_to_init) {
      Serial.println("Initializing...");
      delay(INIT_DELAY);
      if (!ready_to_init) {
        Serial.println("Ready to init changed while waiting...");
        return;
      }
      // Send the current led map
      sendLedMap();

      // Initialize the displays
      initDisplays();

      // Clear spurious events from kb uart buffers
      clearKeyboardBuffers();
      init_done = true;
      // Turn on the status led
      digitalWrite(STATUS_LED, HIGH);
    } else {
      // Turn off the status led
      digitalWrite(STATUS_LED, LOW);
    }
  }
}

keyEvent k;

// Main loop to use when initialized
void mainLoop() {
  if (gameState == 0) {
    // Start new game
    randomSeed(micros());
    uint16_t rnd = random(0, 217);

    k.kb = lcKeys[rnd][0];
    k.key = lcKeys[rnd][1];
    Serial.printf("Got first key kb: %d key %d\n", k.kb, k.key);
    queue_read = 0;
    queue_write = 0;
    queue[queue_write].kb = k.kb;
    queue[queue_write].key = k.key;
    queue_write++;
    queue_write = queue_write % 100;
    gameState = 1;
    // Drain keypresses
    clearKeyboardBuffers();
    initLedMap();
    note_to_led(k.kb, k.key, true);
    gameDelay = 5000;

  } else if (gameState == 1) {
    // Handle the keypresses
    for (uint8_t i = 0; i < 4; i++) {
      while (keyboard_serials[i]->available()) {
        uint8_t keycode = keyboard_serials[i]->read();

        if ((keycode & 0x80) == 0) {
          // Key up
          continue;
        }
        keycode = keycode & 0x7f;
        if (queue[queue_read].kb == (i + 1) && queue[queue_read].key == keycode) {
          // Correct key
          gameTimeout = millis();
          gameScore = 1;
          gameState = 2;
          queue_read++;
          newKey();
          Serial.println("Starting the game!");
          clearKeyboardBuffers();
          return;
        } else {
          // Incorrect key
        }
      }
    }
  } else if (gameState == 2) {
    if (millis() > gameTimeout + gameDelay) {
      // Timeout, get new key
      newKey();
      gameDelay = gameDelay - 100;
      if (gameDelay < 200) {
        gameDelay = 700;
      }
      if (queue_write == queue_read) {
        // Queue full, game over
        gameState = 3;
        return;
      }
    } // End of timeout
    // SCAN FOR NEW KEY
    for (uint8_t i = 0; i < 4; i++) {
      while (keyboard_serials[i]->available()) {
        uint8_t keycode = keyboard_serials[i]->read();
        if ((keycode & 0x80) != 0) {
          // Key up
          Serial.println("key up detected");
          continue;
        }
        keycode = keycode & 0x7f;
        Serial.printf("Got keycode %d %d\n", i + 1, keycode);
        if (queue[queue_read].kb == (i + 1) && queue[queue_read].key == keycode) {
          // Correct key
          gameScore++;
          uint8_t next_read = (queue_read + 1) % 100;
          if (next_read == queue_write) {
            gameTimeout = millis() - gameDelay; // Trigger next cycle
          }
          queue_read++;
          queue_read = queue_read % 100;
          Serial.println("Got correct key");
        } else {
          gameState = 3;
          Serial.printf("Incorrect key %d %d\n", i + 1, keycode);
          return;
        }
      }
    }
  } else if (gameState == 3) {
    // Game over
    Serial.printf("Game over, score: %d\n", gameScore);
    AllLedsOn();
    sendLedMap();
    delay(1000);
    initLedMap();
    gameState = 0;
    if (gameScore > gameHiScore) {
      gameHiScore = gameScore;
    }

  }
  showScore();


  // Process the MIDI events
  // usbMIDI.read();

  // Process the ADC data if a new packet has been received
  if (process_adc_data) {
    processADC();
    process_adc_data = false;
    // sendMidiADC();
  }

  // Process display UART stuff
  handleDisplays();


  // Send updated led data periodically one address at a time
  if (led_interval > LED_INTERVAL) {
    updateLeds();
    led_interval = 0;
  }
}


// General pin setup
void setupPins(void) {
  // Disable the line buffers
  pinMode(CE, OUTPUT);
  digitalWrite(CE, HIGH);

  for (uint8_t i = 0; i < INPUTS_N; i++) {
    pinMode(Inputs[i], INPUT);
  }

  for (uint8_t i = 0; i < OUTPUTS_N; i++) {
    pinMode(Outputs[i], OUTPUT);
  }

  // LCD pins, inactive reset, inactive calibration
  digitalWrite(LCD1_RST, HIGH);
  digitalWrite(LCD2_RST, HIGH);
  digitalWrite(LCD1_CAL, LOW);
  digitalWrite(LCD2_CAL, LOW);
}

// ISR on PIN14_IN falling edge
void detectReset() {
  ready_to_init = false;
  init_done = false;
}

// Debug / Board commissioning function, reads and prints all input states
void printInputs(void) {
  uint8_t val;

  val = digitalRead(LCD1_TX);
  Serial.print("LCD1_TX: ");
  Serial.print(val);

  val = digitalRead(LCD2_TX);
  Serial.print(" LCD2_TX: ");
  Serial.print(val);

  val = digitalRead(ADC_CS);
  Serial.print(" ACD_CS: ");
  Serial.print(val);

  val = digitalRead(ADC_CLK);
  Serial.print(" ACD_CLK: ");
  Serial.print(val);

  val = digitalRead(ADC_DATA);
  Serial.print(" ACD_DATA: ");
  Serial.print(val);

  val = digitalRead(KB_1);
  Serial.print(" KB_1: ");
  Serial.print(val);

  val = digitalRead(KB_2);
  Serial.print(" KB_2: ");
  Serial.print(val);

  val = digitalRead(KB_3);
  Serial.print(" KB_3: ");
  Serial.print(val);

  val = digitalRead(KB_4);
  Serial.print(" KB_4: ");
  Serial.print(val);

  val = digitalRead(PIN14_IN);
  Serial.print(" PIN14_IN: ");
  Serial.print(val);

  val = digitalRead(PIN15_IN);
  Serial.print(" PIN15_IN: ");
  Serial.println(val);
}

void newKey() {
  uint16_t rnd = random(0, 217);
  note_to_led(k.kb, k.key, false); // Disable previous led

  k.kb = lcKeys[rnd][0];
  k.key = lcKeys[rnd][1];
  Serial.printf("Got new key kb: %d key %d\n", k.kb, k.key);
  queue[queue_write].kb = k.kb;
  queue[queue_write].key = k.key;
  queue_write++;
  queue_write = queue_write % 100;
  note_to_led(k.kb, k.key, true); // Show the next led
  gameTimeout = millis();
}

void showScore() {
  uint16_t mask = 1;
  for (int i = 0; i < 15; i++) {
    mask = 1 << i;
    uint8_t chan = scoreLeds[i][0];
    uint8_t led = scoreLeds[i][1];
    if ((gameScore & mask) == 0) {
      note_to_led(chan, led, false);
    } else {
      note_to_led(chan, led, true);
    }
    chan = hiScoreLeds[i][0];
    led = hiScoreLeds[i][1];
    if ((gameHiScore & mask) == 0) {
      note_to_led(chan, led, false);
    } else {
      note_to_led(chan, led, true);
    }
  }
}


// Debug / board commissioning function, cycles all output pins
void cycleOutputs(void) {
  for (uint8_t i = 0; i < OUTPUTS_N; i++) {
    pinMode(Outputs[i], OUTPUT);
    digitalWrite(Outputs[i], HIGH);
    delay(500);
    digitalWrite(Outputs[i], LOW);
  }
}

// prints 8-bit data in hex
void printHex8(uint8_t *data, uint8_t length) {
  char tmp[length * 2 + 1];
  byte first;
  byte second;
  for (int i = 0; i < length; i++) {
    first = (data[i] >> 4) & 0x0f;
    second = data[i] & 0x0f;
    // base for converting single digit numbers to ASCII is 48
    // base for 10-16 to become lower-case characters a-f is 87
    // note: difference is 39
    tmp[i * 2] = first + 48;
    tmp[i * 2 + 1] = second + 48;
    if (first > 9) tmp[i * 2] += 39;
    if (second > 9) tmp[i * 2 + 1] += 39;
  }
  tmp[length * 2] = 0;
  Serial.print(tmp);
}
