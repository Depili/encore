uint8_t led_update_counter = 0;

void initLedSPI(void) {
  pinMode(LED_CS, OUTPUT);
  SPI1.setSCK(LED_CLK);
  SPI1.setMOSI(LED_DATA);
  SPI1.begin();
}

void initLedMap(void) {
  for (uint8_t i = 0; i < LEDS_N; i++) {
    led_map[i] = 0;
  }
}

void AllLedsOn(void) {
  for (uint8_t i = 0; i < LEDS_N; i++) {
    led_map[i] = 0xFFFF;
  }
}



void sendLeds(uint8_t addr, uint8_t *data) {
  uint8_t buff[4];
  buff[0] = data[0];
  buff[1] = data[1];
  buff[2] = data[2];
  buff[3] = data[3];
  digitalWrite(LED_CS, LOW);
  SPI1.beginTransaction(LED_SETTINGS);
  SPI1.transfer(addr);
  SPI1.transfer(buff, 4);
  SPI1.endTransaction();
  digitalWrite(LED_CS, HIGH);
}

void updateLeds(void) {
  noInterrupts();
  sendLeds(LED_ADDR[led_update_counter], (uint8_t *)&led_map[led_update_counter]);
  led_update_counter = (led_update_counter + 1) % LEDS_N;
  interrupts();
}

void sendLedMap(void) {
  for (uint8_t i = 0; i < LEDS_N; i++) {
    sendLeds(LED_ADDR[i], (uint8_t *)led_map[i]);
  }
}

void walkLeds(void) {
  for (uint8_t i = 0; i < 32; i++) {
    uint32_t walk = 0;
    bitSet(walk, i);
    Serial.print("Led bit: ");
    Serial.println(i);

    for (uint8_t j = 0; j < LEDS_N; j++) {
      uint32_t msg = walk;
      sendLeds(LED_ADDR[j], (uint8_t *)&msg);
    }
    while(!Serial.available() ){
    }
    while(Serial.available() ){
      Serial.read();
    }
  }
}

void toggleLeds(uint8_t addr) {
  uint8_t leds_on[4] = {0xFF, 0xFF, 0xFF, 0xFF};
  uint8_t leds_off[4] = {0x00, 0x00, 0x00, 0x00};
  sendLeds(addr, leds_on);
  while(!Serial.available() ){
  }
  while(Serial.available() ){
    Serial.read();
  }
  sendLeds(addr, leds_off);
}
