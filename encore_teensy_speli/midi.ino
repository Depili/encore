
const uint8_t midi_chan_map[10] = {MIDI_JOY_X, MIDI_JOY_Y, MIDI_JOY_Z, MIDI_TBAR,
  MIDI_ENC_1, MIDI_ENC_2, MIDI_ENC_3, MIDI_ENC_4, MIDI_ENC_5, MIDI_ENC_6};


uint8_t prev_controls[11] = {0,0,0,0,0,0,0,0,0,0};

void init_midi(void) {
  usbMIDI.setHandleNoteOff(handleNoteOff);
  usbMIDI.setHandleNoteOn(handleNoteOn);
}

void kb_midi(uint8_t kb_num, uint8_t keycode) {
  bool key_up = false;

  if (bitRead(keycode, 7) == 1) {
    key_up = true;
  }
  bitClear(keycode, 7);

  if (PRINT_MIDI) {
    Serial.print("Sending midi note: 0x");
    printHex8(&keycode, 1);
    Serial.print(" channel: ");
    Serial.print(kb_num);
    Serial.print(" direction: ");
    Serial.println(key_up);
  }


  if (key_up == true) {
    usbMIDI.sendNoteOff(keycode, MIDI_VEL, kb_num);
  } else {
    usbMIDI.sendNoteOn(keycode, MIDI_VEL, kb_num);
  }

  if (MIDI_LOOPBACK) {
    note_to_led(kb_num, keycode, key_up);
  }
}

void handleNoteOff(byte channel, byte note, byte velocity) {
  note_to_led(channel, note, false);
}

void handleNoteOn(byte channel, byte note, byte velocity) {
  note_to_led(channel, note, true);
}

void sendMidiADC(void) {
  uint8_t new_controls[10] = {0,0,0,0,0,0,0,0,0,0};
  new_controls[0] = (uint8_t)(joy_x >> 5);
  new_controls[1] = (uint8_t)(joy_y >> 5);
  new_controls[2] = (uint8_t)(joy_z >> 5);
  new_controls[3] = (uint8_t)(tbar >> 5);

  for (uint8_t i = 0; i < 6; i++) {
    new_controls[4+i] = encoders[i];
  }

  // Compare and send
  for (uint8_t i = 0; i < 4; i++) {
    uint8_t prev = prev_controls[i];
    uint8_t val = new_controls[i];

    if (val < (prev - ADC_DELTA) || val > (prev + ADC_DELTA))  {
      prev_controls[i] = new_controls[i];
      usbMIDI.sendControlChange(midi_chan_map[i], new_controls[i], 1);
    }
  }

  // Encoders
  for (uint8_t i = 0; i < 6; i++) {
    if (new_controls[4+i] != prev_controls[4+i]) {
      prev_controls[4+i] = new_controls[4+i];
      usbMIDI.sendControlChange(midi_chan_map[i+4], new_controls[4+i], 1);
    }
  }
}
