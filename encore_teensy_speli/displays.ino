/*
 * Code for the Amulet LCD modules
 */

/*
 * Serial communication multiplexing
 */

// Supported subset of amulet commands
const uint8_t amulet_cmd_bytes[] = {0xD0, 0xD1, 0xD2, 0xD5, 0xD6, 0xD7, 0xD9, 0xDA, 0xDB, 0xDC, 0xF0, 0xF1};
// Alternative bytes for display 2
const uint8_t amulet_cmd_alt[] =   {0xB0, 0xB1, 0xB2, 0xB5, 0xB6, 0xB7, 0xB9, 0xBA, 0xBB, 0xBC, 0xC0, 0xC1};
// Command lengths in bytes
const uint8_t amulet_cmd_len[] =   {3   , 3   , 3   , 5   , 7   , 0xFF, 19  , 19  , 19  , 11  , 1   , 1   };

// Variables for directing the incoming commands to correct displays
uint8_t lcd_dest = 0;
uint8_t lcd_bytes_remaining = 0;

/*
 * Initialization data for the LCD displays
 */

const uint8_t lcd_set_page_50[] = {0xD6, 'F', 'F','0','0','5','0'}; // Set word variable 0xFF to 0x0050
const uint8_t lcd_write_caption[] = {0xD7, 'C', '5'}; // Write string 0xC5
const char lcd_caption[] = {"Reversed by Depili @ Helsinki Hacklab"}; // Screen topic

void initDisplays() {
  // Wait for the display uart line to rise?
  delay(LCD_INIT_DELAY);
  LCD1_UART.write(lcd_set_page_50, sizeof(lcd_set_page_50));
  LCD2_UART.write(lcd_set_page_50, sizeof(lcd_set_page_50));
  delay(LCD_INIT_DELAY);
  LCD1_UART.write(lcd_write_caption, sizeof(lcd_write_caption));
  LCD1_UART.write(lcd_caption, sizeof(lcd_caption));
  LCD1_UART.write(0x00);
  LCD2_UART.write(lcd_write_caption, sizeof(lcd_write_caption));
  LCD2_UART.write(lcd_caption, sizeof(lcd_caption));
  LCD2_UART.write(0x00);


  for (uint16_t i = 0; i < sizeof(pixels) / sizeof(pixels[0]); i++) {
    LCD1_UART.write(0xDC);
    LCD1_UART.print(pixels[i]);
    LCD2_UART.write(0xDC);
    LCD2_UART.print(pixels[i]);
    delay(LCD_DRAW_DELAY);
  }

  for (uint16_t i = 0; i < sizeof(lines) / sizeof(lines[0]); i++) {
    LCD1_UART.write(0xD9);
    LCD1_UART.print(lines[i]);
    LCD2_UART.write(0xD9);
    LCD2_UART.print(lines[i]);
    delay(LCD_DRAW_DELAY);
  }


  for (uint16_t i = 0; i < sizeof(rectangles) / sizeof(rectangles[0]); i++) {
    LCD1_UART.write(0xDB);
    LCD1_UART.print(rectangles[i]);
    LCD2_UART.write(0xDB);
    LCD2_UART.print(rectangles[i]);
    delay(LCD_DRAW_DELAY);
  }

  
}


/*
 * Multiple display implementation, shift the command bytes around for display 2
 */
void handleDisplays() {
  // Easy case for display 1 receive
  while (LCD1_UART.available()) {
    uint8_t c = LCD1_UART.read();
    Serial.write(c);
  }

  // Harder, need to shift the command bytes...
  while (LCD2_UART.available()) {
    uint8_t c = LCD2_UART.read();
    if (c > 0x80) {
      // Command byte
      for (uint8_t i = 0; i < sizeof(amulet_cmd_bytes); i++) {
        if (c == amulet_cmd_bytes[i]) {
          Serial.write(amulet_cmd_alt[i]);
        }
      }
    } else {
      Serial.write(c);
    }
  }

  // Handle commands being sent from PC to amulets...
  while (Serial.available()) {
    uint8_t c = Serial.read();
    if (lcd_bytes_remaining == 0) {
      // Wait for proper CMD byte
      uint8_t i = lcd_cmd_byte_in(amulet_cmd_bytes, c);
      if (i != 0xFF) {
        // LCD1 message
        lcd_dest = 1;
        lcd_bytes_remaining = amulet_cmd_len[i] - 1;
        LCD1_UART.write(c);
      }
      i = lcd_cmd_byte_in(amulet_cmd_alt, c);
      if (i != 0xFF) {
        // LCD2 message
        lcd_dest = 2;
        lcd_bytes_remaining = amulet_cmd_len[i] - 1;
        LCD2_UART.write(amulet_cmd_bytes[i]);
      }
    } else if (c == 0x00) {
      // Terminate current command
      lcd_bytes_remaining = 0;
      lcd_dest = 0;
    } else if (lcd_dest == 1) {
      LCD1_UART.write(c);
      lcd_bytes_remaining--;
    } else if (lcd_dest == 2) {
      LCD2_UART.write(c);
      lcd_bytes_remaining--;
    } else {
      // Something strange happened...
      lcd_dest = 0;
      lcd_bytes_remaining = 0;
    }
  }
}

uint8_t lcd_cmd_byte_in(const uint8_t *cmd_bytes, uint8_t c) {
  for (uint8_t i = 0; i < sizeof(amulet_cmd_bytes); i++) {
    if (c == cmd_bytes[i]) {
      return i;
    }
  }
  return 0xFF;
}

/*
  Serial relay, write received data to a display
*/
void displayRelay(HardwareSerial *kb_serial) {
  while (kb_serial->available()) {
    uint8_t c = kb_serial->read();
    Serial.write(c);
  }

  while (Serial.available()) {
    uint8_t c = Serial.read();
    kb_serial->write(c);
  }
}

void displaySendWord(HardwareSerial *kb_serial, uint8_t addr, uint16_t value) {
  char msg[7] = {0xD6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  sprintf(&msg[1], "%02X", addr);
  sprintf(&msg[3], "%04X", value);
  kb_serial->write(msg, 7);
}

void displaySendByte(HardwareSerial *kb_serial, uint8_t addr, uint8_t value) {
  char msg[5] = {0xD6, 0x00, 0x00, 0x00, 0x00};
  sprintf(&msg[1], "%02X", addr);
  sprintf(&msg[3], "%02X", value);
  kb_serial->write(msg, 5);
}
