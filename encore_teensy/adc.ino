/*
   Interrupt service routines
*/

// SPI0 data avaible interrupt
// This reads in the ADC data packet
void spi0_isr(void) {
  uint16_t value;
  value = SPI0_POPR;
  adc_data[adc_data_index % 7] = value;
  adc_data_index++;
  SPI0_SR |= SPI_SR_RFDF;
}

/*
   SPI0 CS rising edge interrupt
   If we have a complete 14 byte message
   move it into processing buffer and raise
   the flag.
*/
void latchADC(void) {
  if (!process_adc_data && adc_data_index == 7) {
    for (uint8_t i = 0; i < 7; i++) {
      adc_message[i * 2] = (uint8_t)(adc_data[i] >> 8);
      adc_message[(i * 2) + 1] = (uint8_t)adc_data[i];
    }
    process_adc_data = true;
    ready_to_init = true;
  }
  adc_data_index = 0;
}

/*
   Process the ADC data packet

   Bytes 0 - 4 are a unknown header
   Bytes 5-10 contain the 12 bit adc values:
     - T-bar
     - Joystick X
     - Joystick Y
     - Joystick z (rotate)
   Bytes 11-13 contain pulses for the 6 encoders:
     - 0xF - Clockwise rotation
     - 0x1 - Counter clockwise rotation
     - One nibble per encoder:
       - Left - Top
       - Left - Middle
       - Left - Bottom
       - Right - Top
       - Right - Middle
       - Right - Bottom
*/
void processADC() {
  if (PRINT_ADC_RAW) {
    Serial.print("ADC RAW: ");
    printHex8(adc_message, 14);
    Serial.println("");
  }

  tbar =  ((uint16_t)adc_message[5] << 4) + ((uint16_t)(adc_message[6] >> 4));
  joy_x = ((uint16_t)(adc_message[6] & 0x0F) << 8) + (uint16_t)adc_message[7];
  joy_y = ((uint16_t)adc_message[8] << 4) + ((uint16_t)(adc_message[9] >> 4));
  joy_z = ((uint16_t)(adc_message[9] & 0x0F) << 8) + (uint16_t)adc_message[10];
  for (uint8_t i = 0; i < 3; i++) {
    if ((adc_message[11 + i] & 0xF0) == 0xF0) encoders[2 * i]++;
    if ((adc_message[11 + i] & 0xF0) == 0x10) encoders[2 * i]--;

    if ((adc_message[11 + i] & 0x0F) == 0x0F) encoders[(2 * i) + 1]++;
    if ((adc_message[11 + i] & 0x0F) == 0x01) encoders[(2 * i) + 1]--;
  }
  if (PRINT_ADC_VALUES) {
    Serial.print(tbar);
    Serial.print(" ");
    Serial.print(joy_x);
    Serial.print(" ");
    Serial.print(joy_y);
    Serial.print(" ");
    Serial.print(joy_z);

    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(" ");
      Serial.print(encoders[i]);
    }
    Serial.println("");
  }
}



/*
   Intialize SPI1 as a SPI slave for receiving the
   ADC data (t-bar, joystick, encoders).
   SPI_MODE3

   Process the received data via interrupts.
   Complete state transmission takes ~120usec
   and repeats every 500usecs

   Initialization mostly from t3spi and TSPISLAVE
   libraries, both had their own issues...

*/
void spi0SlaveSetup() {
  SIM_SCGC6 |= SIM_SCGC6_SPI0; // enable slave clock

  spi_map = 0x4002C000;
  spi_irq = IRQ_SPI0;

  (*(KINETISK_SPI_t *)spi_map).MCR |= SPI_MCR_HALT | SPI_MCR_MDIS;
  (*(KINETISK_SPI_t *)spi_map).MCR = 0x00000000;
  (*(KINETISK_SPI_t *)spi_map).MCR &= ~SPI_MCR_HALT & ~SPI_MCR_MDIS;
  (*(KINETISK_SPI_t *)spi_map).CTAR0 = 0;
  (*(KINETISK_SPI_t *)spi_map).MCR |= SPI_MCR_HALT | SPI_MCR_MDIS;

  // Read data in 16 bit chuncks (you need to give size -1.. go figure..)
  (*(KINETISK_SPI_t *)spi_map).CTAR0 = SPI_CTAR_FMSZ(15);

  (*(KINETISK_SPI_t *)spi_map).MCR &= ~SPI_MCR_HALT & ~SPI_MCR_MDIS;
  (*(KINETISK_SPI_t *)spi_map).MCR |= SPI_MCR_HALT | SPI_MCR_MDIS;

  // We need SPI mode 3, ie. inverted clock and clock polarity 1.
  (*(KINETISK_SPI_t *)spi_map).CTAR0 = (*(KINETISK_SPI_t *)spi_map).CTAR0 | SPI_CTAR_CPOL | SPI_CTAR_CPHA;

  // SPI mode 0
  //    (*(KINETISK_SPI_t *)spi_map).CTAR0 = (*(KINETISK_SPI_t *)spi_map).CTAR0 & ~(SPI_CTAR_CPOL | SPI_CTAR_CPHA);

  (*(KINETISK_SPI_t *)spi_map).MCR &= ~SPI_MCR_HALT & ~SPI_MCR_MDIS;
  (*(KINETISK_SPI_t *)spi_map).RSER = 0x00020000;

  // Pins:
  // CLK = 13, MOSI = 11 MISO = 12 CS = 2
  // Clock
  CORE_PIN13_CONFIG = PORT_PCR_MUX(2);
  // MOSI
  CORE_PIN11_CONFIG = PORT_PCR_DSE | PORT_PCR_MUX(2);
  // MISO, this is actually used as the save for data in...
  CORE_PIN12_CONFIG = PORT_PCR_MUX(2);
  // Use alternative CS pin 2 as we need serial2
  CORE_PIN2_CONFIG =  PORT_PCR_PS | PORT_PCR_MUX(2);

  SPI0_MCR &= ~SPI_MCR_HALT & ~SPI_MCR_MDIS; // start

  // Interrupts:

  // CS interrupt
  NVIC_SET_PRIORITY(spi_irq, 1); // set priority
  NVIC_ENABLE_IRQ(IRQ_SPI0); // enable CS IRQ

  // Interrupt on the CS line going high at the end
  // of transaction
  attachInterrupt(2, latchADC, RISING);
}
