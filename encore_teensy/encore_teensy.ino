/*
   Teensy 3.5 program for using the Barco Folsom Encore LC and SC keyboards

   Plug the Teensy to the ribbon cable that is normally between the carrier board
   and the keyboard.

   PINS:

  TODO: add pin table
  TODO: Route pins 14 and 15 in and re-init on their changes?
*/

#include "SPI.h"
#include "encore.h"

// ADC input via SPI MODE3 slave and interrupts
volatile uint32_t spi_map;
volatile uint32_t spi_irq;
uint8_t adc_message[14];
uint16_t adc_data[7];
volatile uint8_t adc_data_index = 0;
volatile bool process_adc_data = false;
// Decoded ADC values
uint16_t tbar, joy_x, joy_y, joy_z;
int16_t encoders[6] = {0, 0, 0, 0, 0, 0};

// LED control via SPI MODE3 master
elapsedMicros led_interval;   // Update interval for leds
uint32_t led_map[LEDS_N];     // Led bitmap

// Initialization flags
volatile bool ready_to_init = false;
volatile bool init_done = false;

void setup() {
  // USB serial
  Serial.begin(115200);

  // General input/output setup for pins
  setupPins();

  // Keyboards
  KB_1_UART.begin(KB_UART_MODE);
  KB_2_UART.begin(KB_UART_MODE);
  KB_3_UART.begin(KB_UART_MODE);
  KB_4_UART.begin(KB_UART_MODE);

  // SPI0 in SPI_MODE3 as a slave
  spi0SlaveSetup();

  // Led control
  initLedSPI();
  initLedMap();

  // Displays
  LCD1_UART.begin(LCD_UART_MODE);
  LCD2_UART.begin(LCD_UART_MODE);

  // Enable line buffers
  digitalWrite(CE, LOW);

  // Send the led bitmap and reset the counter
  sendLedMap();
  led_interval = 0;

  // Initialize the usb-midi interface
  init_midi();

  attachInterrupt(PIN14_IN, detectReset, FALLING);
}

void loop() {
  
  if (init_done) {
      // Run the main processing loop
      mainLoop();
  } else {
    if (ready_to_init) {
      Serial.println("Initializing...");
      delay(INIT_DELAY);
      if (!ready_to_init) {
        Serial.println("Ready to init changed while waiting...");
        return;
      }
      // Send the current led map
      sendLedMap();
      
      // Initialize the displays
      initDisplays();

      // Clear spurious events from kb uart buffers
      clearKeyboardBuffers();
      init_done = true;
      // Turn on the status led
      digitalWrite(STATUS_LED, HIGH);
    } else {
      // Turn off the status led
      digitalWrite(STATUS_LED, LOW);
    }
  }
}

// Main loop to use when initialized
void mainLoop() {
  // Process the MIDI events
  usbMIDI.read();

  // Process the ADC data if a new packet has been received
  if (process_adc_data) {
    processADC();
    process_adc_data = false;
    sendMidiADC();
  }

  // Process display UART stuff
  handleDisplays();

  // Handle the keypresses
  processKeyboard();

  // Send updated led data periodically one address at a time
  if (led_interval > LED_INTERVAL) {
    updateLeds();
    led_interval = 0;
  }  
}


// General pin setup
void setupPins(void) {
  // Disable the line buffers
  pinMode(CE, OUTPUT);
  digitalWrite(CE, HIGH);

  for (uint8_t i = 0; i < INPUTS_N; i++) {
    pinMode(Inputs[i], INPUT);
  }

  for (uint8_t i = 0; i < OUTPUTS_N; i++) {
    pinMode(Outputs[i], OUTPUT);
  }

  // LCD pins, inactive reset, inactive calibration
  digitalWrite(LCD1_RST, HIGH);
  digitalWrite(LCD2_RST, HIGH);
  digitalWrite(LCD1_CAL, LOW);
  digitalWrite(LCD2_CAL, LOW);
}

// ISR on PIN14_IN falling edge
void detectReset() {
  ready_to_init = false;
  init_done = false;
}

// Debug / Board commissioning function, reads and prints all input states
void printInputs(void) {
  uint8_t val;

  val = digitalRead(LCD1_TX);
  Serial.print("LCD1_TX: ");
  Serial.print(val);

  val = digitalRead(LCD2_TX);
  Serial.print(" LCD2_TX: ");
  Serial.print(val);

  val = digitalRead(ADC_CS);
  Serial.print(" ACD_CS: ");
  Serial.print(val);

  val = digitalRead(ADC_CLK);
  Serial.print(" ACD_CLK: ");
  Serial.print(val);

  val = digitalRead(ADC_DATA);
  Serial.print(" ACD_DATA: ");
  Serial.print(val);

  val = digitalRead(KB_1);
  Serial.print(" KB_1: ");
  Serial.print(val);

  val = digitalRead(KB_2);
  Serial.print(" KB_2: ");
  Serial.print(val);

  val = digitalRead(KB_3);
  Serial.print(" KB_3: ");
  Serial.print(val);

  val = digitalRead(KB_4);
  Serial.print(" KB_4: ");
  Serial.print(val);

  val = digitalRead(PIN14_IN);
  Serial.print(" PIN14_IN: ");
  Serial.print(val);

  val = digitalRead(PIN15_IN);
  Serial.print(" PIN15_IN: ");
  Serial.println(val);
}


// Debug / board commissioning function, cycles all output pins
void cycleOutputs(void) {
  for (uint8_t i = 0; i < OUTPUTS_N; i++) {
    pinMode(Outputs[i], OUTPUT);
    digitalWrite(Outputs[i], HIGH);
    delay(500);
    digitalWrite(Outputs[i], LOW);
  }
}

// prints 8-bit data in hex
void printHex8(uint8_t *data, uint8_t length) {
  char tmp[length * 2 + 1];
  byte first;
  byte second;
  for (int i = 0; i < length; i++) {
    first = (data[i] >> 4) & 0x0f;
    second = data[i] & 0x0f;
    // base for converting single digit numbers to ASCII is 48
    // base for 10-16 to become lower-case characters a-f is 87
    // note: difference is 39
    tmp[i * 2] = first + 48;
    tmp[i * 2 + 1] = second + 48;
    if (first > 9) tmp[i * 2] += 39;
    if (second > 9) tmp[i * 2 + 1] += 39;
  }
  tmp[length * 2] = 0;
  Serial.print(tmp);
}
