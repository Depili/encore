// Pointer array of all 4 keyboard serial ports
HardwareSerial *keyboard_serials[] = {&KB_1_UART, &KB_2_UART, &KB_3_UART, &KB_4_UART};

// Read keyboard events and send them as midi commands
void processKeyboard(void) {  
  for (uint8_t i = 0; i < 4; i++) {
    while(keyboard_serials[i]->available()) {
      uint8_t keycode = keyboard_serials[i]->read();
      kb_midi(i+1, keycode);    
    }
  }
}

// Clear all bytes in keyboard serial receive buffers
void clearKeyboardBuffers() {
  for (uint8_t i = 0; i < 4; i++) {
    while (keyboard_serials[i]->available()) {
      keyboard_serials[i]->read();
    }
  }
}
