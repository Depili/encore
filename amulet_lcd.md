## Amulet variables

### General

The display is 320x240 pixels resistive touch screen.

The "status page" (`0xFF = 0x0050`) has a heading and 9 lines of length 42 characters. Longer strings wrap if there are rows under the current line.

The menu page (`0xFF = 0x0010`) can have a heading, 8 tabs and 9 lines of 32 characters and 3 labels for the encoders. The tabs and lines generate events when touched.

Both the status page and the menu page can have a popup window over it by writing `0xFF = 0x00FE`. The popup window needs to be cancelled with `0xFF = 0xFFFF` or other page transitions are ignored.

#### Strings

Strings are 7bit ascii only. Valid characters seem to be 0x0C (line feed) and 0x20-0x79.

#### Font style escape

Amulet StringField Widgets can statically set the font style of the dynamic string at compile time. The font styles available are plain, bold, italic, underline and strikethrough. If it is desired to change the font style at runtime, that can be done by using the font style escape character `0x02`. The byte following the font style escape character determines the font style of the characters that follow. It is not sent as an ASCII character, but rather a raw byte determined by the font style or styles chosen. Each font style is represented by a single bit within the font style byte. Multiple font styles can be specified at one time, except in the case of plain, which must stand alone.

Style bits:
* 0x01 - bold
* 0x02 - italic
* 0x04 - strikethrough
* 0x08 - underline
* 0x80 - plain

### Amulet word FF values

* 0001 - submenu touch buttons (SC or old without user keys)
* 0002 - submenu touch buttons (LC or with user keys)
* 0010 - Main menu with tabs
* 0050 - Status display page
* 00FE - Popup
* 5A5A - ??
* FFFF - Popup off

Setting the page variable to invalid number results in a blank page.

### Strings:
* 0x9A - Popup text, will be typeset into lines automatically.
* 0xA0 - First tab, text must be set before changing to page 0x0010
* 0xA1 - Second tab...
* 0xA7 - Last tab
* 0xA8 - Top encoder label
* 0xA9 - Middle encoder label
* 0xAA - Bottom encoder label
* 0xB0 - First line of text in menus / status page
* 0xB2 - Second line
* 0xB4 - Third..
* 0xC0 - 9th line
* 0xC5 - Status page heading, changing the page clears

### Bytes:
0xA0 - 0xA7 - Toggles for menu tabs
0xB0 - 0xB8 - Highlight text row in menu
0x40 - Contrast?
0x41 - Brightness?

### Menu touch events:

The events arrive as set byte variable calls from the amulet. The address is 0xFF.
* F0 - F7 Menu tabs
* 00 - 08 Menu text lines

### Page 0x0001 and 0x0002 touch events:

Events arrive as set word variable calls from the amulet, aimed to address 0xFF. The payload is:
* 0x0020 - INPUT
* 0x0021 - OUTPUT
* 0x0022 - SYSTEM
* 0x0023 - MISC
* 0x0024 - PRESETS
* 0x0025 - EFFECTS
* 0x0026 - SEQS
* 0x0027 - USER KEYS
* 0x0028 - FSN CONTROL
