EESchema Schematic File Version 4
LIBS:encore_teensy_v2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2450 1550 0    50   Input ~ 0
LCD1_RX
Text HLabel 2450 1750 0    50   Input ~ 0
LCD1_CAL
Text HLabel 2450 1850 0    50   Input ~ 0
LCD1_RST
Text HLabel 2450 1950 0    50   Input ~ 0
LCD2_RX
Text HLabel 2450 2150 0    50   Input ~ 0
LCD2_CAL
Text HLabel 2450 2250 0    50   Input ~ 0
LCD2_RST
Text HLabel 2450 2900 0    50   Input ~ 0
LED_CS
Text HLabel 2450 3000 0    50   Input ~ 0
LED_DATA
Text HLabel 2450 3100 0    50   Input ~ 0
LED_CLK
Text HLabel 2500 5550 0    50   Input ~ 0
KB_JTAG_TCK
Text HLabel 2500 5650 0    50   Input ~ 0
KB_JTAG_MS
Text HLabel 2500 5850 0    50   Input ~ 0
KB_JTAG_TDI
Text HLabel 3400 5750 2    50   Output ~ 0
KB_JTAG_TDO
Text HLabel 3200 4900 2    50   Output ~ 0
KB_4
Text HLabel 3200 4800 2    50   Output ~ 0
KB_3
Text HLabel 3200 4700 2    50   Output ~ 0
KB_2
Text HLabel 3200 4600 2    50   Output ~ 0
KB_1
Text HLabel 3200 4000 2    50   Output ~ 0
ADC_CS
Text HLabel 3200 3900 2    50   Output ~ 0
ADC_DATA
Text HLabel 3200 3800 2    50   Output ~ 0
ADC_CLK
Text HLabel 3250 2050 2    50   Output ~ 0
LCD2_TX
Text HLabel 3250 1650 2    50   Output ~ 0
LCD1_TX
Text HLabel 4500 3800 0    50   3State ~ 0
PIN_14
Text HLabel 4500 3900 0    50   3State ~ 0
PIN_15
Text GLabel 6250 4850 3    50   UnSpc ~ 0
GND
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 1 1 5DB225A4
P 2600 1350
F 0 "J1" H 2850 1515 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 2850 1424 50  0000 C CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 2550 1600 50  0001 C CNN
F 3 "" H 2550 1600 50  0001 C CNN
	1    2600 1350
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 2 1 5DB25591
P 6000 4400
F 0 "J1" H 6528 4213 50  0000 L CNN
F 1 "EC_KB_CONN_V2" H 6528 4122 50  0000 L CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 5950 4650 50  0001 C CNN
F 3 "" H 5950 4650 50  0001 C CNN
	2    6000 4400
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 4 1 5DB2C8AA
P 2600 3600
F 0 "J1" H 2908 3765 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 2908 3674 50  0000 C CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 2550 3850 50  0001 C CNN
F 3 "" H 2550 3850 50  0001 C CNN
	4    2600 3600
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 5 1 5DB305A5
P 2750 4400
F 0 "J1" H 2983 4565 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 2983 4474 50  0000 C CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 2700 4650 50  0001 C CNN
F 3 "" H 2700 4650 50  0001 C CNN
	5    2750 4400
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 6 1 5DB32309
P 2650 5350
F 0 "J1" H 2950 5515 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 2950 5424 50  0000 C CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 2600 5600 50  0001 C CNN
F 3 "" H 2600 5600 50  0001 C CNN
	6    2650 5350
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 7 1 5DB380A8
P 4650 3600
F 0 "J1" H 5319 3471 50  0000 L CNN
F 1 "EC_KB_CONN_V2" H 5319 3380 50  0000 L CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 4600 3850 50  0001 C CNN
F 3 "" H 4600 3850 50  0001 C CNN
	7    4650 3600
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_EC_KB_CONN_V2 J1
U 3 1 5DB291FD
P 2600 2700
F 0 "J1" H 3078 2521 50  0000 L CNN
F 1 "EC_KB_CONN_V2" H 3078 2430 50  0000 L CNN
F 2 "SamacSys_Parts:SAMTEC_EHF-120-01-L-D-SM" H 2550 2950 50  0001 C CNN
F 3 "" H 2550 2950 50  0001 C CNN
	3    2600 2700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
