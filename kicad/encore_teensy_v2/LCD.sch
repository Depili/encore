EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L barco~encore:teensy3.5 U?
U 3 1 5D6E0E13
P 2450 4350
AR Path="/5D6E0E13" Ref="U?"  Part="3" 
AR Path="/5D6DCCA8/5D6E0E13" Ref="U?"  Part="3" 
F 0 "U" H 2700 3750 50  0000 C CNN
F 1 "teensy3.5" H 2650 4400 50  0000 C CNN
F 2 "" H 2700 3750 50  0001 C CNN
F 3 "" H 2700 3750 50  0001 C CNN
	3    2450 4350
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:teensy3.5 U?
U 2 1 5D6E0E19
P 2450 3550
AR Path="/5D6E0E19" Ref="U?"  Part="2" 
AR Path="/5D6DCCA8/5D6E0E19" Ref="U?"  Part="2" 
F 0 "U" H 2700 2950 50  0000 C CNN
F 1 "teensy3.5" H 2650 3600 50  0000 C CNN
F 2 "" H 2700 2950 50  0001 C CNN
F 3 "" H 2700 2950 50  0001 C CNN
	2    2450 3550
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:EC_KB_CONN_V2 J?
U 8 1 5D6E0E1F
P 7150 4050
AR Path="/5D6E0E1F" Ref="J?"  Part="8" 
AR Path="/5D6DCCA8/5D6E0E1F" Ref="J?"  Part="8" 
F 0 "J" H 7700 3200 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 7650 4100 50  0000 C CNN
F 2 "" H 7500 4300 50  0001 C CNN
F 3 "" H 7500 4300 50  0001 C CNN
	8    7150 4050
	1    0    0    -1  
$EndComp
$Comp
L barco~encore:EC_KB_CONN_V2 J?
U 1 1 5D6E0E25
P 7200 2700
AR Path="/5D6E0E25" Ref="J?"  Part="1" 
AR Path="/5D6DCCA8/5D6E0E25" Ref="J?"  Part="1" 
F 0 "J" H 7750 1850 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 7700 2750 50  0000 C CNN
F 2 "" H 7550 2950 50  0001 C CNN
F 3 "" H 7550 2950 50  0001 C CNN
	1    7200 2700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U?
U 1 1 5D6E387D
P 4800 3650
F 0 "U" H 4500 4300 50  0000 C CNN
F 1 "74HC245" H 4500 3000 50  0000 C CNN
F 2 "" H 4800 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 4800 3650 50  0001 C CNN
	1    4800 3650
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U?
U 1 1 5D6E460E
P 4800 5600
F 0 "U" H 4500 6250 50  0000 C CNN
F 1 "74HC245" H 4500 4950 50  0000 C CNN
F 2 "" H 4800 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 4800 5600 50  0001 C CNN
	1    4800 5600
	1    0    0    -1  
$EndComp
Text HLabel 4800 2850 0    50   Input ~ 0
Vcc
Text HLabel 4800 4800 0    50   Input ~ 0
Vcc
Text HLabel 4800 4450 0    50   Input ~ 0
GND
Text HLabel 4800 6400 0    50   Input ~ 0
GND
$Comp
L Device:R R?
U 1 1 5D6E70C4
P 3850 4200
F 0 "R" V 3930 4200 50  0000 C CNN
F 1 "R" V 3850 4200 50  0000 C CNN
F 2 "" V 3780 4200 50  0001 C CNN
F 3 "~" H 3850 4200 50  0001 C CNN
	1    3850 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D6E7BBC
P 3850 6150
F 0 "R" V 3930 6150 50  0000 C CNN
F 1 "R" V 3850 6150 50  0000 C CNN
F 2 "" V 3780 6150 50  0001 C CNN
F 3 "~" H 3850 6150 50  0001 C CNN
	1    3850 6150
	1    0    0    -1  
$EndComp
Text HLabel 4300 4150 0    50   Input ~ 0
OE
Text HLabel 4300 6100 0    50   Input ~ 0
OE
Wire Wire Line
	3850 4050 4300 4050
Wire Wire Line
	3850 6000 4300 6000
Text HLabel 3850 4350 0    50   Input ~ 0
GND
Text HLabel 3850 6300 0    50   Input ~ 0
Vcc
Wire Wire Line
	2650 3150 4300 3150
Wire Wire Line
	2650 3950 3550 3950
Wire Wire Line
	3550 3950 3550 3250
Wire Wire Line
	3550 3250 4300 3250
Wire Wire Line
	7050 3150 5300 3150
Wire Wire Line
	6500 3250 6500 4500
Wire Wire Line
	6500 4500 7000 4500
Wire Wire Line
	5300 3250 6500 3250
Wire Wire Line
	7050 3050 6100 3050
Wire Wire Line
	6100 3050 6100 5100
Wire Wire Line
	6100 5100 5300 5100
Wire Wire Line
	7050 3350 6200 3350
Wire Wire Line
	6200 3350 6200 5200
Wire Wire Line
	6200 5200 5300 5200
Wire Wire Line
	7050 3450 6300 3450
Wire Wire Line
	6300 3450 6300 5300
Wire Wire Line
	6300 5300 5300 5300
Wire Wire Line
	7000 4400 6600 4400
Wire Wire Line
	6600 4400 6600 5400
Wire Wire Line
	6600 5400 5300 5400
Wire Wire Line
	7000 4700 6700 4700
Wire Wire Line
	6700 4700 6700 5500
Wire Wire Line
	6700 5500 5300 5500
Wire Wire Line
	7000 4800 6800 4800
Wire Wire Line
	6800 4800 6800 5600
Wire Wire Line
	6800 5600 5300 5600
Wire Wire Line
	4300 5100 3450 5100
Wire Wire Line
	3450 5100 3450 3050
Wire Wire Line
	3450 3050 2650 3050
Wire Wire Line
	4300 5200 3350 5200
Wire Wire Line
	3350 5200 3350 3250
Wire Wire Line
	3350 3250 2650 3250
Wire Wire Line
	4300 5300 3250 5300
Wire Wire Line
	3250 5300 3250 3350
Wire Wire Line
	3250 3350 2650 3350
Wire Wire Line
	4300 5400 3150 5400
Wire Wire Line
	3150 5400 3150 3850
Wire Wire Line
	3150 3850 2650 3850
Wire Wire Line
	4300 5500 3050 5500
Wire Wire Line
	3050 5500 3050 4050
Wire Wire Line
	3050 4050 2650 4050
Wire Wire Line
	4300 5600 2950 5600
Wire Wire Line
	2950 5600 2950 4150
Wire Wire Line
	2950 4150 2650 4150
$EndSCHEMATC
