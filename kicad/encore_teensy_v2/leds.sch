EESchema Schematic File Version 4
LIBS:encore_teensy_v2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L archive:Device_LED_ALT D1
U 1 1 5DAB2F86
P 1650 1000
F 0 "D1" H 1643 745 50  0000 C CNN
F 1 "PWR" H 1643 836 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 1000 50  0001 C CNN
F 3 "~" H 1650 1000 50  0001 C CNN
	1    1650 1000
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R4
U 1 1 5DAB41F2
P 2100 1000
F 0 "R4" V 1893 1000 50  0000 C CNN
F 1 "600" V 1984 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 1000 50  0001 C CNN
F 3 "~" H 2100 1000 50  0001 C CNN
	1    2100 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1000 1950 1000
Text HLabel 1500 1400 0    50   Input ~ 0
STATUS
$Comp
L archive:Device_LED_ALT D2
U 1 1 5DAB604E
P 1650 1400
F 0 "D2" H 1643 1145 50  0000 C CNN
F 1 "STATUS" H 1643 1236 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 1400 50  0001 C CNN
F 3 "~" H 1650 1400 50  0001 C CNN
	1    1650 1400
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R5
U 1 1 5DAB6058
P 2100 1400
F 0 "R5" V 1893 1400 50  0000 C CNN
F 1 "600" V 1984 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 1400 50  0001 C CNN
F 3 "~" H 2100 1400 50  0001 C CNN
	1    2100 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1400 1950 1400
$Comp
L archive:Device_LED_ALT D3
U 1 1 5DAB7FAD
P 1650 2150
F 0 "D3" H 1643 1895 50  0000 C CNN
F 1 "LCD1_TX" H 1643 1986 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 2150 50  0001 C CNN
F 3 "~" H 1650 2150 50  0001 C CNN
	1    1650 2150
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R6
U 1 1 5DAB7FB7
P 2100 2150
F 0 "R6" V 1893 2150 50  0000 C CNN
F 1 "600" V 1984 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 2150 50  0001 C CNN
F 3 "~" H 2100 2150 50  0001 C CNN
	1    2100 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 2150 1950 2150
Text HLabel 1500 2150 0    50   Input ~ 0
LCD1_TX
$Comp
L archive:Device_LED_ALT D4
U 1 1 5DABACC3
P 1650 2500
F 0 "D4" H 1643 2245 50  0000 C CNN
F 1 "LCD1_RX" H 1643 2336 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 2500 50  0001 C CNN
F 3 "~" H 1650 2500 50  0001 C CNN
	1    1650 2500
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R7
U 1 1 5DABACCD
P 2100 2500
F 0 "R7" V 1893 2500 50  0000 C CNN
F 1 "600" V 1984 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 2500 50  0001 C CNN
F 3 "~" H 2100 2500 50  0001 C CNN
	1    2100 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 2500 1950 2500
$Comp
L archive:Device_LED_ALT D5
U 1 1 5DABCDEB
P 1650 2850
F 0 "D5" H 1643 2595 50  0000 C CNN
F 1 "LCD1_CAL" H 1643 2686 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 2850 50  0001 C CNN
F 3 "~" H 1650 2850 50  0001 C CNN
	1    1650 2850
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R8
U 1 1 5DABCDF1
P 2100 2850
F 0 "R8" V 1893 2850 50  0000 C CNN
F 1 "600" V 1984 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 2850 50  0001 C CNN
F 3 "~" H 2100 2850 50  0001 C CNN
	1    2100 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 2850 1950 2850
$Comp
L archive:Device_LED_ALT D6
U 1 1 5DABDC31
P 1650 3200
F 0 "D6" H 1643 2945 50  0000 C CNN
F 1 "LCD1_RST" H 1643 3036 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 3200 50  0001 C CNN
F 3 "~" H 1650 3200 50  0001 C CNN
	1    1650 3200
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R9
U 1 1 5DABDC37
P 2100 3200
F 0 "R9" V 1893 3200 50  0000 C CNN
F 1 "600" V 1984 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 3200 50  0001 C CNN
F 3 "~" H 2100 3200 50  0001 C CNN
	1    2100 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 3200 1950 3200
$Comp
L archive:Device_LED_ALT D7
U 1 1 5DAC2297
P 1650 3700
F 0 "D7" H 1643 3445 50  0000 C CNN
F 1 "LCD2_TX" H 1643 3536 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 3700 50  0001 C CNN
F 3 "~" H 1650 3700 50  0001 C CNN
	1    1650 3700
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R10
U 1 1 5DAC229D
P 2100 3700
F 0 "R10" V 1893 3700 50  0000 C CNN
F 1 "600" V 1984 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 3700 50  0001 C CNN
F 3 "~" H 2100 3700 50  0001 C CNN
	1    2100 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 3700 1950 3700
$Comp
L archive:Device_LED_ALT D8
U 1 1 5DAC22A4
P 1650 4050
F 0 "D8" H 1643 3795 50  0000 C CNN
F 1 "LCD2_RX" H 1643 3886 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 4050 50  0001 C CNN
F 3 "~" H 1650 4050 50  0001 C CNN
	1    1650 4050
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R11
U 1 1 5DAC22AA
P 2100 4050
F 0 "R11" V 1893 4050 50  0000 C CNN
F 1 "600" V 1984 4050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 4050 50  0001 C CNN
F 3 "~" H 2100 4050 50  0001 C CNN
	1    2100 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4050 1950 4050
$Comp
L archive:Device_LED_ALT D9
U 1 1 5DAC22B1
P 1650 4400
F 0 "D9" H 1643 4145 50  0000 C CNN
F 1 "LCD2_CAL" H 1643 4236 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 4400 50  0001 C CNN
F 3 "~" H 1650 4400 50  0001 C CNN
	1    1650 4400
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R12
U 1 1 5DAC22B7
P 2100 4400
F 0 "R12" V 1893 4400 50  0000 C CNN
F 1 "600" V 1984 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 4400 50  0001 C CNN
F 3 "~" H 2100 4400 50  0001 C CNN
	1    2100 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4400 1950 4400
$Comp
L archive:Device_LED_ALT D10
U 1 1 5DAC22BE
P 1650 4750
F 0 "D10" H 1643 4495 50  0000 C CNN
F 1 "LCD2_RST" H 1643 4586 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 4750 50  0001 C CNN
F 3 "~" H 1650 4750 50  0001 C CNN
	1    1650 4750
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R13
U 1 1 5DAC22C4
P 2100 4750
F 0 "R13" V 1893 4750 50  0000 C CNN
F 1 "600" V 1984 4750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 4750 50  0001 C CNN
F 3 "~" H 2100 4750 50  0001 C CNN
	1    2100 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4750 1950 4750
Text HLabel 1500 2500 0    50   Input ~ 0
LCD1_RX
Text HLabel 1500 2850 0    50   Input ~ 0
LCD1_CAL
Text HLabel 1500 3200 0    50   Input ~ 0
LCD1_RST
Text HLabel 1500 3700 0    50   Input ~ 0
LCD2_TX
Text HLabel 1500 4050 0    50   Input ~ 0
LCD2_RX
Text HLabel 1500 4400 0    50   Input ~ 0
LCD2_CAL
Text HLabel 1500 4750 0    50   Input ~ 0
LCD2_RST
$Comp
L archive:Device_LED_ALT D11
U 1 1 5DAC508F
P 1650 5300
F 0 "D11" H 1643 5045 50  0000 C CNN
F 1 "LED_CS" H 1643 5136 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 5300 50  0001 C CNN
F 3 "~" H 1650 5300 50  0001 C CNN
	1    1650 5300
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R14
U 1 1 5DAC5095
P 2100 5300
F 0 "R14" V 1893 5300 50  0000 C CNN
F 1 "600" V 1984 5300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 5300 50  0001 C CNN
F 3 "~" H 2100 5300 50  0001 C CNN
	1    2100 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 5300 1950 5300
$Comp
L archive:Device_LED_ALT D12
U 1 1 5DAC509C
P 1650 5650
F 0 "D12" H 1643 5395 50  0000 C CNN
F 1 "LED_DATA" H 1643 5486 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 5650 50  0001 C CNN
F 3 "~" H 1650 5650 50  0001 C CNN
	1    1650 5650
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R15
U 1 1 5DAC50A2
P 2100 5650
F 0 "R15" V 1893 5650 50  0000 C CNN
F 1 "600" V 1984 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 5650 50  0001 C CNN
F 3 "~" H 2100 5650 50  0001 C CNN
	1    2100 5650
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 5650 1950 5650
$Comp
L archive:Device_LED_ALT D13
U 1 1 5DAC50A9
P 1650 6000
F 0 "D13" H 1643 5745 50  0000 C CNN
F 1 "LED_CLK" H 1643 5836 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 6000 50  0001 C CNN
F 3 "~" H 1650 6000 50  0001 C CNN
	1    1650 6000
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R16
U 1 1 5DAC50AF
P 2100 6000
F 0 "R16" V 1893 6000 50  0000 C CNN
F 1 "600" V 1984 6000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 6000 50  0001 C CNN
F 3 "~" H 2100 6000 50  0001 C CNN
	1    2100 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6000 1950 6000
$Comp
L archive:Device_LED_ALT D14
U 1 1 5DAC9098
P 1650 6500
F 0 "D14" H 1643 6245 50  0000 C CNN
F 1 "ADC_CS" H 1643 6336 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 6500 50  0001 C CNN
F 3 "~" H 1650 6500 50  0001 C CNN
	1    1650 6500
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R17
U 1 1 5DAC909E
P 2100 6500
F 0 "R17" V 1893 6500 50  0000 C CNN
F 1 "600" V 1984 6500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 6500 50  0001 C CNN
F 3 "~" H 2100 6500 50  0001 C CNN
	1    2100 6500
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6500 1950 6500
$Comp
L archive:Device_LED_ALT D15
U 1 1 5DAC90A5
P 1650 6850
F 0 "D15" H 1643 6595 50  0000 C CNN
F 1 "ADC_DATA" H 1643 6686 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 6850 50  0001 C CNN
F 3 "~" H 1650 6850 50  0001 C CNN
	1    1650 6850
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R18
U 1 1 5DAC90AB
P 2100 6850
F 0 "R18" V 1893 6850 50  0000 C CNN
F 1 "600" V 1984 6850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 6850 50  0001 C CNN
F 3 "~" H 2100 6850 50  0001 C CNN
	1    2100 6850
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6850 1950 6850
$Comp
L archive:Device_LED_ALT D16
U 1 1 5DAC90B2
P 1650 7200
F 0 "D16" H 1643 6945 50  0000 C CNN
F 1 "ADC_CLK" H 1643 7036 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 1650 7200 50  0001 C CNN
F 3 "~" H 1650 7200 50  0001 C CNN
	1    1650 7200
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R19
U 1 1 5DAC90B8
P 2100 7200
F 0 "R19" V 1893 7200 50  0000 C CNN
F 1 "600" V 1984 7200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2030 7200 50  0001 C CNN
F 3 "~" H 2100 7200 50  0001 C CNN
	1    2100 7200
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 7200 1950 7200
Text HLabel 1500 5300 0    50   Input ~ 0
LED_CS
Text HLabel 1500 5650 0    50   Input ~ 0
LED_DATA
Text HLabel 1500 6000 0    50   Input ~ 0
LED_CLK
Text HLabel 1500 6500 0    50   Input ~ 0
ADC_CS
Text HLabel 1500 6850 0    50   Input ~ 0
ADC_DATA
Text HLabel 1500 7200 0    50   Input ~ 0
ADC_CLK
$Comp
L archive:Device_LED_ALT D19
U 1 1 5DAD01DC
P 3400 2150
F 0 "D19" H 3393 1895 50  0000 C CNN
F 1 "KB_1" H 3393 1986 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 2150 50  0001 C CNN
F 3 "~" H 3400 2150 50  0001 C CNN
	1    3400 2150
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R22
U 1 1 5DAD01E6
P 3850 2150
F 0 "R22" V 3643 2150 50  0000 C CNN
F 1 "600" V 3734 2150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 2150 50  0001 C CNN
F 3 "~" H 3850 2150 50  0001 C CNN
	1    3850 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 2150 3700 2150
$Comp
L archive:Device_LED_ALT D20
U 1 1 5DAD01F1
P 3400 2500
F 0 "D20" H 3393 2245 50  0000 C CNN
F 1 "KB_2" H 3393 2336 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 2500 50  0001 C CNN
F 3 "~" H 3400 2500 50  0001 C CNN
	1    3400 2500
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R23
U 1 1 5DAD01FB
P 3850 2500
F 0 "R23" V 3643 2500 50  0000 C CNN
F 1 "600" V 3734 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 2500 50  0001 C CNN
F 3 "~" H 3850 2500 50  0001 C CNN
	1    3850 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 2500 3700 2500
$Comp
L archive:Device_LED_ALT D21
U 1 1 5DAD0206
P 3400 2850
F 0 "D21" H 3393 2595 50  0000 C CNN
F 1 "KB_3" H 3393 2686 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 2850 50  0001 C CNN
F 3 "~" H 3400 2850 50  0001 C CNN
	1    3400 2850
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R24
U 1 1 5DAD0210
P 3850 2850
F 0 "R24" V 3643 2850 50  0000 C CNN
F 1 "600" V 3734 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 2850 50  0001 C CNN
F 3 "~" H 3850 2850 50  0001 C CNN
	1    3850 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 2850 3700 2850
$Comp
L archive:Device_LED_ALT D22
U 1 1 5DAD021B
P 3400 3200
F 0 "D22" H 3393 2945 50  0000 C CNN
F 1 "KB_4" H 3393 3036 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 3200 50  0001 C CNN
F 3 "~" H 3400 3200 50  0001 C CNN
	1    3400 3200
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R25
U 1 1 5DAD0225
P 3850 3200
F 0 "R25" V 3643 3200 50  0000 C CNN
F 1 "600" V 3734 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 3200 50  0001 C CNN
F 3 "~" H 3850 3200 50  0001 C CNN
	1    3850 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 3200 3700 3200
Text HLabel 3250 2150 0    50   Input ~ 0
KB_1
Text HLabel 3250 2500 0    50   Input ~ 0
KB_2
Text HLabel 3250 2850 0    50   Input ~ 0
KB_3
Text HLabel 3250 3200 0    50   Input ~ 0
KB_4
$Comp
L archive:Device_LED_ALT D17
U 1 1 5DADC726
P 3400 1000
F 0 "D17" H 3393 745 50  0000 C CNN
F 1 "PIN_14" H 3393 836 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 1000 50  0001 C CNN
F 3 "~" H 3400 1000 50  0001 C CNN
	1    3400 1000
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R20
U 1 1 5DADC730
P 3850 1000
F 0 "R20" V 3643 1000 50  0000 C CNN
F 1 "600" V 3734 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 1000 50  0001 C CNN
F 3 "~" H 3850 1000 50  0001 C CNN
	1    3850 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 1000 3700 1000
$Comp
L archive:Device_LED_ALT D18
U 1 1 5DADC73B
P 3400 1400
F 0 "D18" H 3393 1145 50  0000 C CNN
F 1 "PIN_15" H 3393 1236 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 1400 50  0001 C CNN
F 3 "~" H 3400 1400 50  0001 C CNN
	1    3400 1400
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R21
U 1 1 5DADC745
P 3850 1400
F 0 "R21" V 3643 1400 50  0000 C CNN
F 1 "600" V 3734 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 1400 50  0001 C CNN
F 3 "~" H 3850 1400 50  0001 C CNN
	1    3850 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 1400 3700 1400
Text HLabel 3250 1400 0    50   Input ~ 0
PIN_15
Text HLabel 3250 1000 0    50   Input ~ 0
PIN_14
$Comp
L archive:Device_LED_ALT D23
U 1 1 5DAE0F55
P 3400 3700
F 0 "D23" H 3393 3445 50  0000 C CNN
F 1 "TDI" H 3393 3536 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 3700 50  0001 C CNN
F 3 "~" H 3400 3700 50  0001 C CNN
	1    3400 3700
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R26
U 1 1 5DAE0F5F
P 3850 3700
F 0 "R26" V 3643 3700 50  0000 C CNN
F 1 "600" V 3734 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 3700 50  0001 C CNN
F 3 "~" H 3850 3700 50  0001 C CNN
	1    3850 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 3700 3700 3700
$Comp
L archive:Device_LED_ALT D24
U 1 1 5DAE0F6A
P 3400 4050
F 0 "D24" H 3393 3795 50  0000 C CNN
F 1 "TDO" H 3393 3886 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 4050 50  0001 C CNN
F 3 "~" H 3400 4050 50  0001 C CNN
	1    3400 4050
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R27
U 1 1 5DAE0F74
P 3850 4050
F 0 "R27" V 3643 4050 50  0000 C CNN
F 1 "600" V 3734 4050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 4050 50  0001 C CNN
F 3 "~" H 3850 4050 50  0001 C CNN
	1    3850 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 4050 3700 4050
$Comp
L archive:Device_LED_ALT D25
U 1 1 5DAE0F7F
P 3400 4400
F 0 "D25" H 3393 4145 50  0000 C CNN
F 1 "TCK" H 3393 4236 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 4400 50  0001 C CNN
F 3 "~" H 3400 4400 50  0001 C CNN
	1    3400 4400
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R28
U 1 1 5DAE0F89
P 3850 4400
F 0 "R28" V 3643 4400 50  0000 C CNN
F 1 "600" V 3734 4400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 4400 50  0001 C CNN
F 3 "~" H 3850 4400 50  0001 C CNN
	1    3850 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 4400 3700 4400
$Comp
L archive:Device_LED_ALT D26
U 1 1 5DAE0F94
P 3400 4750
F 0 "D26" H 3393 4495 50  0000 C CNN
F 1 "MS" H 3393 4586 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" H 3400 4750 50  0001 C CNN
F 3 "~" H 3400 4750 50  0001 C CNN
	1    3400 4750
	-1   0    0    1   
$EndComp
$Comp
L archive:Device_R R29
U 1 1 5DAE0F9E
P 3850 4750
F 0 "R29" V 3643 4750 50  0000 C CNN
F 1 "600" V 3734 4750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3780 4750 50  0001 C CNN
F 3 "~" H 3850 4750 50  0001 C CNN
	1    3850 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 4750 3700 4750
Text HLabel 3250 3700 0    50   Input ~ 0
KB_JTAG_TDI
Text HLabel 3250 4050 0    50   Input ~ 0
KB_JTAG_TDO
Text HLabel 3250 4400 0    50   Input ~ 0
KB_JTAG_TCK
Text HLabel 3250 4750 0    50   Input ~ 0
KB_JTAG_MS
Text GLabel 2350 1200 2    50   UnSpc ~ 0
GND
Wire Wire Line
	2250 1000 2250 1200
Wire Wire Line
	2350 1200 2250 1200
Connection ~ 2250 1200
Wire Wire Line
	2250 1200 2250 1400
$Comp
L archive:Connector_Generic_Conn_01x02 J5
U 1 1 5DAF1844
P 4400 5100
F 0 "J5" V 4364 4912 50  0000 R CNN
F 1 "Conn_01x02" V 4273 4912 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 4400 5100 50  0001 C CNN
F 3 "~" H 4400 5100 50  0001 C CNN
	1    4400 5100
	0    -1   -1   0   
$EndComp
Text GLabel 4500 5450 3    50   UnSpc ~ 0
GND
Wire Wire Line
	4500 5300 4500 5450
Text GLabel 1500 1000 0    50   Input ~ 0
5V
Wire Wire Line
	2250 5300 2300 5300
Wire Wire Line
	4000 4750 4100 4750
Wire Wire Line
	4100 4750 4100 5300
Connection ~ 4100 5300
Wire Wire Line
	4100 5300 4400 5300
Wire Wire Line
	4100 4750 4100 4400
Wire Wire Line
	4100 4400 4000 4400
Connection ~ 4100 4750
Wire Wire Line
	4100 4400 4100 4050
Wire Wire Line
	4100 4050 4000 4050
Connection ~ 4100 4400
Wire Wire Line
	4100 4050 4100 3700
Wire Wire Line
	4100 3700 4000 3700
Connection ~ 4100 4050
Wire Wire Line
	4100 3700 4100 3200
Wire Wire Line
	4100 3200 4000 3200
Connection ~ 4100 3700
Wire Wire Line
	4100 3200 4100 2850
Wire Wire Line
	4100 2850 4000 2850
Connection ~ 4100 3200
Wire Wire Line
	4100 2850 4100 2500
Wire Wire Line
	4100 2500 4000 2500
Connection ~ 4100 2850
Wire Wire Line
	4100 2500 4100 2150
Wire Wire Line
	4100 2150 4000 2150
Connection ~ 4100 2500
Wire Wire Line
	2250 5650 2300 5650
Wire Wire Line
	2300 5650 2300 5300
Connection ~ 2300 5300
Wire Wire Line
	2300 5300 4100 5300
Wire Wire Line
	2250 6000 2300 6000
Wire Wire Line
	2300 6000 2300 5650
Connection ~ 2300 5650
Wire Wire Line
	2250 6500 2300 6500
Wire Wire Line
	2300 6500 2300 6000
Connection ~ 2300 6000
Wire Wire Line
	2250 6850 2300 6850
Wire Wire Line
	2300 6850 2300 6500
Connection ~ 2300 6500
Wire Wire Line
	2250 7200 2300 7200
Wire Wire Line
	2300 7200 2300 6850
Connection ~ 2300 6850
Wire Wire Line
	2250 4750 2300 4750
Wire Wire Line
	2300 4750 2300 5300
Wire Wire Line
	2250 4400 2300 4400
Wire Wire Line
	2300 4400 2300 4750
Connection ~ 2300 4750
Wire Wire Line
	2250 4050 2300 4050
Wire Wire Line
	2300 4050 2300 4400
Connection ~ 2300 4400
Wire Wire Line
	2250 3700 2300 3700
Wire Wire Line
	2300 3700 2300 4050
Connection ~ 2300 4050
Wire Wire Line
	2250 3200 2300 3200
Wire Wire Line
	2300 3200 2300 3700
Connection ~ 2300 3700
Wire Wire Line
	2250 2850 2300 2850
Wire Wire Line
	2300 2850 2300 3200
Connection ~ 2300 3200
Wire Wire Line
	2250 2500 2300 2500
Wire Wire Line
	2300 2500 2300 2850
Connection ~ 2300 2850
Wire Wire Line
	2250 2150 2300 2150
Wire Wire Line
	2300 2150 2300 2500
Connection ~ 2300 2500
Wire Wire Line
	4000 1400 4100 1400
Wire Wire Line
	4100 1400 4100 2150
Connection ~ 4100 2150
Wire Wire Line
	4000 1000 4100 1000
Wire Wire Line
	4100 1000 4100 1400
Connection ~ 4100 1400
Text Label 3900 5300 2    50   ~ 0
LED_ENABLE
$EndSCHEMATC
