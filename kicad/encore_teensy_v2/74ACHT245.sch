EESchema Schematic File Version 4
LIBS:encore_teensy_v2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Encore SC & LC adapter for teensy 3.5"
Date "2019-08-25"
Rev ""
Comp "Helsinki Hacklab"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L archive:74xx_74HC245 U2
U 1 1 5D703BD2
P 5450 3400
AR Path="/5D703ACF/5D703BD2" Ref="U2"  Part="1" 
AR Path="/5D745F7F/5D703BD2" Ref="U3"  Part="1" 
AR Path="/5D7FB37E/5D703BD2" Ref="U4"  Part="1" 
F 0 "U2" H 5450 4381 50  0000 C CNN
F 1 "74HC245" H 5450 4290 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm" H 5450 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 5450 3400 50  0001 C CNN
	1    5450 3400
	1    0    0    -1  
$EndComp
$Comp
L archive:Device_R R1
U 1 1 5D704DD4
P 4600 3950
AR Path="/5D703ACF/5D704DD4" Ref="R1"  Part="1" 
AR Path="/5D745F7F/5D704DD4" Ref="R2"  Part="1" 
AR Path="/5D7FB37E/5D704DD4" Ref="R3"  Part="1" 
F 0 "R1" H 4670 3996 50  0000 L CNN
F 1 "10k" H 4670 3905 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4530 3950 50  0001 C CNN
F 3 "~" H 4600 3950 50  0001 C CNN
	1    4600 3950
	1    0    0    -1  
$EndComp
Text HLabel 4500 4200 0    50   Input ~ 0
GND
Text HLabel 4500 2600 0    50   Input ~ 0
VCC
Text HLabel 4950 3900 0    50   Input ~ 0
CE
Text HLabel 5950 2900 2    50   Input ~ 0
IN1
Text HLabel 5950 3000 2    50   Input ~ 0
IN2
Text HLabel 5950 3100 2    50   Input ~ 0
IN3
Text HLabel 5950 3200 2    50   Input ~ 0
IN4
Text HLabel 5950 3300 2    50   Input ~ 0
IN5
Text HLabel 5950 3400 2    50   Input ~ 0
IN6
Text HLabel 5950 3500 2    50   Input ~ 0
IN7
Text HLabel 5950 3600 2    50   Input ~ 0
IN8
Text HLabel 4950 2900 0    50   Output ~ 0
OUT1
Text HLabel 4950 3000 0    50   Output ~ 0
OUT2
Text HLabel 4950 3100 0    50   Output ~ 0
OUT3
Text HLabel 4950 3200 0    50   Output ~ 0
OUT4
Text HLabel 4950 3300 0    50   Output ~ 0
OUT5
Text HLabel 4950 3400 0    50   Output ~ 0
OUT6
Text HLabel 4950 3500 0    50   Output ~ 0
OUT7
Text HLabel 4950 3600 0    50   Output ~ 0
OUT8
Wire Wire Line
	4500 2600 4850 2600
Wire Wire Line
	4500 4200 4600 4200
Wire Wire Line
	4600 4200 4600 4100
Wire Wire Line
	4600 4200 5450 4200
Connection ~ 4600 4200
Wire Wire Line
	4600 3800 4950 3800
$Comp
L archive:Device_C C1
U 1 1 5D7E69E9
P 4850 2450
AR Path="/5D703ACF/5D7E69E9" Ref="C1"  Part="1" 
AR Path="/5D745F7F/5D7E69E9" Ref="C2"  Part="1" 
AR Path="/5D7FB37E/5D7E69E9" Ref="C3"  Part="1" 
F 0 "C1" H 4965 2496 50  0000 L CNN
F 1 "100n" H 4965 2405 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4888 2300 50  0001 C CNN
F 3 "~" H 4850 2450 50  0001 C CNN
	1    4850 2450
	1    0    0    -1  
$EndComp
Text HLabel 4500 2300 0    50   Input ~ 0
GND
Wire Wire Line
	4500 2300 4850 2300
Connection ~ 4850 2600
Wire Wire Line
	4850 2600 5450 2600
$EndSCHEMATC
