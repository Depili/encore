EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L barco~encore:teensy3.5 U?
U 8 1 5D6CEF4B
P 5400 3250
AR Path="/5D6CEF4B" Ref="U?"  Part="8" 
AR Path="/5D6C283B/5D6CEF4B" Ref="U?"  Part="8" 
F 0 "U?" H 5650 2650 50  0000 C CNN
F 1 "teensy3.5" H 5600 3300 50  0000 C CNN
F 2 "" H 5650 2650 50  0001 C CNN
F 3 "" H 5650 2650 50  0001 C CNN
	8    5400 3250
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:EC_KB_CONN_V2 J?
U 4 1 5D6CEF51
P 7300 2200
AR Path="/5D6CEF51" Ref="J?"  Part="4" 
AR Path="/5D6C283B/5D6CEF51" Ref="J?"  Part="4" 
F 0 "J?" H 7850 1350 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 7800 2250 50  0000 C CNN
F 2 "" H 7650 2450 50  0001 C CNN
F 3 "" H 7650 2450 50  0001 C CNN
	4    7300 2200
	1    0    0    -1  
$EndComp
$Comp
L barco~encore:EC_KB_CONN_V2 J?
U 5 1 5D6CEF57
P 7300 3300
AR Path="/5D6CEF57" Ref="J?"  Part="5" 
AR Path="/5D6C283B/5D6CEF57" Ref="J?"  Part="5" 
F 0 "J?" H 7850 2450 50  0000 C CNN
F 1 "EC_KB_CONN_V2" H 7800 3350 50  0000 C CNN
F 2 "" H 7650 3550 50  0001 C CNN
F 3 "" H 7650 3550 50  0001 C CNN
	5    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U?
U 1 1 5D6CEF5D
P 6250 3250
AR Path="/5D6CEF5D" Ref="U?"  Part="1" 
AR Path="/5D6C283B/5D6CEF5D" Ref="U?"  Part="1" 
F 0 "U?" H 5950 3900 50  0000 C CNN
F 1 "74HC245" H 5950 2600 50  0000 C CNN
F 2 "" H 6250 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 6250 3250 50  0001 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2750 7150 2750
Wire Wire Line
	6750 2850 7150 2850
Wire Wire Line
	6750 2950 7150 2950
Wire Wire Line
	7150 3050 6750 3050
Wire Wire Line
	7050 3150 6750 3150
Wire Wire Line
	6950 3250 6750 3250
Wire Wire Line
	6850 3350 6750 3350
Text GLabel 5750 3050 0    50   Input ~ 0
KB_1
Text GLabel 5750 3150 0    50   Input ~ 0
KB_2
Text GLabel 5750 3250 0    50   Input ~ 0
KB_3
Text GLabel 5750 3350 0    50   Input ~ 0
KB_4
Wire Wire Line
	5600 2950 5750 2950
Wire Wire Line
	5600 2850 5750 2850
Wire Wire Line
	5600 2750 5750 2750
Wire Wire Line
	7150 3050 7150 3750
Wire Wire Line
	7050 3850 7150 3850
Wire Wire Line
	7050 3150 7050 3850
Wire Wire Line
	6950 3950 7150 3950
Wire Wire Line
	6950 3250 6950 3950
Wire Wire Line
	6850 4050 7150 4050
Wire Wire Line
	6850 3350 6850 4050
Text HLabel 6250 2450 1    50   Input ~ 0
Vcc
Text HLabel 5400 4150 0    50   Input ~ 0
GND
Text HLabel 5750 3750 0    50   Input ~ 0
OE
$Comp
L Device:R R?
U 1 1 5D6D0BF3
P 5400 3900
F 0 "R?" H 5470 3946 50  0000 L CNN
F 1 "10k" H 5470 3855 50  0000 L CNN
F 2 "" V 5330 3900 50  0001 C CNN
F 3 "~" H 5400 3900 50  0001 C CNN
	1    5400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3650 5400 3650
Wire Wire Line
	5400 3650 5400 3750
Wire Wire Line
	5400 4050 5400 4150
Wire Wire Line
	5400 4150 6250 4150
Wire Wire Line
	6250 4150 6250 4050
$Comp
L barco~encore:teensy3.5 U?
U 4 1 5D6DC020
P 3300 2350
F 0 "U?" H 3550 1750 50  0000 C CNN
F 1 "teensy3.5" H 3500 2400 50  0000 C CNN
F 2 "" H 3550 1750 50  0001 C CNN
F 3 "" H 3550 1750 50  0001 C CNN
	4    3300 2350
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:teensy3.5 U?
U 5 1 5D6DC026
P 3300 3100
F 0 "U?" H 3550 2500 50  0000 C CNN
F 1 "teensy3.5" H 3500 3150 50  0000 C CNN
F 2 "" H 3550 2500 50  0001 C CNN
F 3 "" H 3550 2500 50  0001 C CNN
	5    3300 3100
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:teensy3.5 U?
U 6 1 5D6DC02C
P 3300 3850
F 0 "U?" H 3550 3250 50  0000 C CNN
F 1 "teensy3.5" H 3500 3900 50  0000 C CNN
F 2 "" H 3550 3250 50  0001 C CNN
F 3 "" H 3550 3250 50  0001 C CNN
	6    3300 3850
	-1   0    0    1   
$EndComp
$Comp
L barco~encore:teensy3.5 U?
U 7 1 5D6DC032
P 3300 4600
F 0 "U?" H 3550 4000 50  0000 C CNN
F 1 "teensy3.5" H 3500 4650 50  0000 C CNN
F 2 "" H 3550 4000 50  0001 C CNN
F 3 "" H 3550 4000 50  0001 C CNN
	7    3300 4600
	-1   0    0    1   
$EndComp
Text GLabel 3500 1950 2    50   Input ~ 0
KB_1
Text GLabel 3500 2700 2    50   Input ~ 0
KB_2
Text GLabel 3500 3450 2    50   Input ~ 0
KB_3
Text GLabel 3500 4200 2    50   Input ~ 0
KB_4
$EndSCHEMATC
