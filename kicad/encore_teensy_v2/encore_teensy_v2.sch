EESchema Schematic File Version 4
LIBS:encore_teensy_v2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Encore SC & LC adapter for teensy 3.5"
Date "2019-09-27"
Rev "3"
Comp "Helsinki Hacklab"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2350 1250 700  1250
U 5D703ACF
F0 "Line buffer" 50
F1 "74ACHT245.sch" 50
F2 "GND" I L 2350 2400 50 
F3 "VCC" I L 2350 2300 50 
F4 "CE" I L 2350 2200 50 
F5 "IN1" I L 2350 1300 50 
F6 "IN2" I L 2350 1400 50 
F7 "IN3" I L 2350 1500 50 
F8 "IN4" I L 2350 1600 50 
F9 "IN5" I L 2350 1700 50 
F10 "IN6" I L 2350 1800 50 
F11 "IN7" I L 2350 1900 50 
F12 "IN8" I L 2350 2000 50 
F13 "OUT1" O R 3050 1300 50 
F14 "OUT2" O R 3050 1400 50 
F15 "OUT3" O R 3050 1500 50 
F16 "OUT4" O R 3050 1600 50 
F17 "OUT5" O R 3050 1700 50 
F18 "OUT6" O R 3050 1800 50 
F19 "OUT7" O R 3050 1900 50 
F20 "OUT8" O R 3050 2000 50 
$EndSheet
Wire Wire Line
	3050 1300 3450 1300
Wire Wire Line
	3050 1400 3450 1400
Wire Wire Line
	3050 1500 3450 1500
Wire Wire Line
	3050 1600 3450 1600
$Sheet
S 2350 3100 700  1250
U 5D745F7F
F0 "Line buffer 2" 50
F1 "74ACHT245.sch" 50
F2 "GND" I L 2350 4250 50 
F3 "VCC" I L 2350 4150 50 
F4 "CE" I L 2350 4050 50 
F5 "IN1" I L 2350 3150 50 
F6 "IN2" I L 2350 3250 50 
F7 "IN3" I L 2350 3350 50 
F8 "IN4" I L 2350 3450 50 
F9 "IN5" I L 2350 3550 50 
F10 "IN6" I L 2350 3650 50 
F11 "IN7" I L 2350 3750 50 
F12 "IN8" I L 2350 3850 50 
F13 "OUT1" O R 3050 3150 50 
F14 "OUT2" O R 3050 3250 50 
F15 "OUT3" O R 3050 3350 50 
F16 "OUT4" O R 3050 3450 50 
F17 "OUT5" O R 3050 3550 50 
F18 "OUT6" O R 3050 3650 50 
F19 "OUT7" O R 3050 3750 50 
F20 "OUT8" O R 3050 3850 50 
$EndSheet
Wire Wire Line
	3050 3250 3150 3250
Wire Wire Line
	2300 3250 2350 3250
Wire Wire Line
	2300 3650 2350 3650
Wire Wire Line
	3050 3650 3150 3650
$Comp
L archive:Connector_Conn_ARM_JTAG_SWD_10 J4
U 1 1 5D7EB5F0
P 5600 5200
F 0 "J4" H 5157 5246 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 5157 5155 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch2.54mm" H 5600 5200 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 5250 3950 50  0001 C CNN
	1    5600 5200
	1    0    0    -1  
$EndComp
$Sheet
S 2350 4950 700  1250
U 5D7FB37E
F0 "Line buffer 3" 50
F1 "74ACHT245.sch" 50
F2 "GND" I L 2350 6100 50 
F3 "VCC" I L 2350 6000 50 
F4 "CE" I L 2350 5900 50 
F5 "IN1" I L 2350 5000 50 
F6 "IN2" I L 2350 5100 50 
F7 "IN3" I L 2350 5200 50 
F8 "IN4" I L 2350 5300 50 
F9 "IN5" I L 2350 5400 50 
F10 "IN6" I L 2350 5500 50 
F11 "IN7" I L 2350 5600 50 
F12 "IN8" I L 2350 5700 50 
F13 "OUT1" O R 3050 5000 50 
F14 "OUT2" O R 3050 5100 50 
F15 "OUT3" O R 3050 5200 50 
F16 "OUT4" O R 3050 5300 50 
F17 "OUT5" O R 3050 5400 50 
F18 "OUT6" O R 3050 5500 50 
F19 "OUT7" O R 3050 5600 50 
F20 "OUT8" O R 3050 5700 50 
$EndSheet
Wire Wire Line
	2000 5000 2350 5000
Wire Wire Line
	2000 5100 2350 5100
Wire Wire Line
	2000 5200 2350 5200
Text GLabel 6050 6750 2    50   Output ~ 0
5V
Text GLabel 6050 7550 2    50   Output ~ 0
GND
Text GLabel 5050 5800 0    50   Input ~ 0
GND
Text GLabel 5050 4600 0    50   Input ~ 0
5V
Wire Wire Line
	5050 4600 5600 4600
Wire Wire Line
	5050 5800 5500 5800
Wire Wire Line
	5500 5800 5600 5800
Connection ~ 5500 5800
Text GLabel 2250 6000 0    50   Input ~ 0
5V
Text GLabel 2250 6100 0    50   Input ~ 0
GND
Text GLabel 2250 2300 0    50   Input ~ 0
5V
Text GLabel 2250 2400 0    50   Input ~ 0
GND
Text GLabel 2250 4150 0    50   Input ~ 0
5V
Text GLabel 2250 4250 0    50   Input ~ 0
GND
Wire Wire Line
	2250 5900 2350 5900
Wire Wire Line
	2250 6000 2350 6000
Wire Wire Line
	2250 6100 2350 6100
Wire Wire Line
	2250 2400 2350 2400
Wire Wire Line
	2250 2300 2350 2300
Wire Wire Line
	2250 2200 2350 2200
Wire Wire Line
	2250 4250 2350 4250
Wire Wire Line
	2250 4150 2350 4150
Wire Wire Line
	2250 4050 2350 4050
Wire Wire Line
	4700 7450 4700 6750
NoConn ~ 3050 1700
NoConn ~ 2350 1700
NoConn ~ 6100 4900
NoConn ~ 2350 5700
NoConn ~ 3050 5700
Text Notes 2400 1050 0    50   ~ 0
ADC, encoders and keyboard\nEncore -> Teensy
Text Notes 2450 2950 0    50   ~ 0
LCD control\nMixed
Text Notes 2400 4800 0    50   ~ 0
Leds\nTeensy -> Encore
Text Notes 5150 6500 0    50   ~ 0
Power
Text Notes 7400 800  0    50   ~ 0
CE for line buffers and\nunknown pins 14 and 15\n
Text Notes 5200 4500 0    50   ~ 0
Keyboard FPGA JTAG
$Comp
L archive:Connector_Generic_Conn_02x03_Odd_Even J3
U 1 1 5D8B5A85
P 9750 1200
F 0 "J3" H 9800 1517 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9800 1426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 9750 1200 50  0001 C CNN
F 3 "~" H 9750 1200 50  0001 C CNN
	1    9750 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5400 2350 5400
Wire Wire Line
	2250 5300 2350 5300
Wire Wire Line
	3050 5600 3150 5600
Wire Wire Line
	3050 5500 3150 5500
Wire Wire Line
	2250 5600 2350 5600
Wire Wire Line
	2250 5500 2350 5500
Wire Wire Line
	3050 5400 3150 5400
Wire Wire Line
	3050 5300 3150 5300
Text Notes 10450 800  2    50   ~ 0
In/Out selection for unknown pins
Wire Wire Line
	3050 1800 4000 1800
Wire Wire Line
	3050 1900 4000 1900
Wire Wire Line
	3050 2000 4000 2000
$Sheet
S 9400 3200 650  3000
U 5D891887
F0 "Encore connector" 50
F1 "EC_conn.sch" 50
F2 "LCD1_RX" I L 9400 3250 50 
F3 "LCD1_CAL" I L 9400 3450 50 
F4 "LCD1_RST" I L 9400 3550 50 
F5 "LCD2_RX" I L 9400 3700 50 
F6 "LCD2_CAL" I L 9400 3900 50 
F7 "LCD2_RST" I L 9400 4000 50 
F8 "LED_CS" I L 9400 4200 50 
F9 "LED_DATA" I L 9400 4300 50 
F10 "LED_CLK" I L 9400 4400 50 
F11 "KB_JTAG_TCK" I L 9400 6100 50 
F12 "KB_JTAG_MS" I L 9400 6000 50 
F13 "KB_JTAG_TDI" I L 9400 5900 50 
F14 "KB_JTAG_TDO" O R 10050 5800 50 
F15 "KB_4" O R 10050 5600 50 
F16 "KB_3" O R 10050 5500 50 
F17 "KB_2" O R 10050 5400 50 
F18 "KB_1" O R 10050 5300 50 
F19 "ADC_CS" O R 10050 4600 50 
F20 "ADC_DATA" O R 10050 4700 50 
F21 "ADC_CLK" O R 10050 4800 50 
F22 "LCD2_TX" O R 10050 3800 50 
F23 "LCD1_TX" O R 10050 3350 50 
F24 "PIN_14" T R 10050 5000 50 
F25 "PIN_15" T R 10050 5100 50 
$EndSheet
Text Label 9300 4000 2    50   ~ 0
LCD2_RST
Text Label 9300 3900 2    50   ~ 0
LCD2_CAL
Text Label 9300 3700 2    50   ~ 0
LCD2_RX
Text Label 9300 3550 2    50   ~ 0
LCD1_RST
Text Label 9300 3450 2    50   ~ 0
LCD1_CAL
Text Label 9300 3250 2    50   ~ 0
LCD1_RX
Wire Wire Line
	9300 3250 9400 3250
Wire Wire Line
	9300 3450 9400 3450
Wire Wire Line
	9300 3550 9400 3550
Text Label 9300 4200 2    50   ~ 0
LED_CS
Text Label 9300 4300 2    50   ~ 0
LED_DATA
Text Label 9300 4400 2    50   ~ 0
LED_CLK
Text Label 9300 5900 2    50   ~ 0
KB_JTAG_TDI
Text Label 9300 6000 2    50   ~ 0
KB_JTAG_MS
Text Label 9300 6100 2    50   ~ 0
KB_JTAG_TCK
Text Label 10150 5600 0    50   ~ 0
KB_4
Text Label 10150 5500 0    50   ~ 0
KB_3
Text Label 10150 5400 0    50   ~ 0
KB_2
Text Label 10150 5300 0    50   ~ 0
KB_1
Text Label 10150 5100 0    50   ~ 0
PIN_15
Text Label 10150 5000 0    50   ~ 0
PIN_14
Text Label 10150 4800 0    50   ~ 0
ADC_CLK
Text Label 10150 4700 0    50   ~ 0
ADC_DATA
Text Label 10150 4600 0    50   ~ 0
ADC_CS
Text Label 10150 3800 0    50   ~ 0
LCD2_TX
Text Label 10150 3350 0    50   ~ 0
LCD1_TX
Wire Wire Line
	9300 4200 9400 4200
Wire Wire Line
	9400 4300 9300 4300
Wire Wire Line
	10050 5300 10150 5300
Wire Wire Line
	10050 5400 10150 5400
Wire Wire Line
	10050 5500 10150 5500
Wire Wire Line
	10050 5600 10150 5600
Wire Wire Line
	9300 5900 9400 5900
Wire Wire Line
	9300 6000 9400 6000
Wire Wire Line
	9300 6100 9400 6100
Text Label 10150 5800 0    50   ~ 0
KB_JTAG_TDO
Wire Wire Line
	10050 5800 10150 5800
Wire Wire Line
	10050 4600 10150 4600
Wire Wire Line
	10050 4700 10150 4700
Wire Wire Line
	10050 3350 10150 3350
Text Label 6200 5100 0    50   ~ 0
KB_JTAG_TCK
Text Label 6200 5200 0    50   ~ 0
KB_JTAG_MS
Text Label 6200 5300 0    50   ~ 0
KB_JTAG_TDO
Text Label 6200 5400 0    50   ~ 0
KB_JTAG_TDI
Wire Wire Line
	6200 5400 6100 5400
Wire Wire Line
	6100 5300 6200 5300
Wire Wire Line
	6100 5200 6200 5200
Wire Wire Line
	6100 5100 6200 5100
Text Label 2250 1800 2    50   ~ 0
ADC_CS
Text Label 2250 1900 2    50   ~ 0
ADC_DATA
Text Label 2250 2000 2    50   ~ 0
ADC_CLK
Wire Wire Line
	2250 2000 2350 2000
Wire Wire Line
	2250 1900 2350 1900
Wire Wire Line
	2250 1800 2350 1800
Text Label 2250 1300 2    50   ~ 0
KB_1
Text Label 2250 1400 2    50   ~ 0
KB_2
Text Label 2250 1500 2    50   ~ 0
KB_3
Text Label 2250 1600 2    50   ~ 0
KB_4
Wire Wire Line
	2250 1300 2350 1300
Wire Wire Line
	2350 1400 2250 1400
Wire Wire Line
	2250 1500 2350 1500
Wire Wire Line
	2250 1600 2350 1600
Text Label 2300 3250 2    50   ~ 0
LCD1_TX
Text Label 2300 3650 2    50   ~ 0
LCD2_TX
Text Label 3150 3150 0    50   ~ 0
LCD1_RX
Text Label 4250 3350 0    50   ~ 0
LCD1_CAL
Text Label 3150 3450 0    50   ~ 0
LCD1_RST
Text Label 3150 3550 0    50   ~ 0
LCD2_RX
Text Label 4250 3750 0    50   ~ 0
LCD2_CAL
Text Label 3150 3850 0    50   ~ 0
LCD2_RST
Wire Wire Line
	3050 3150 3150 3150
Wire Wire Line
	3050 3450 3150 3450
Wire Wire Line
	3050 3550 3150 3550
Wire Wire Line
	3150 3850 3050 3850
Text Label 3150 3250 0    50   ~ 0
T_RX1
Text Label 3150 3650 0    50   ~ 0
T_RX2
Text Label 900  3250 2    50   ~ 0
T_RX1
Text Label 900  3650 2    50   ~ 0
T_RX2
Text Label 3200 5000 0    50   ~ 0
LED_CS
Text Label 3200 5100 0    50   ~ 0
LED_DATA
Text Label 3200 5200 0    50   ~ 0
LED_CLK
Wire Wire Line
	3050 5000 3200 5000
Wire Wire Line
	3050 5100 3200 5100
Wire Wire Line
	3050 5200 3200 5200
Text Label 10050 1200 0    50   ~ 0
PIN_15
Text Label 10050 1100 0    50   ~ 0
PIN_15_IN
Text Label 10050 1300 0    50   ~ 0
PIN_15_OUT
Text Label 9550 1100 2    50   ~ 0
PIN_14_IN
Text Label 9550 1200 2    50   ~ 0
PIN_14
Text Label 9550 1300 2    50   ~ 0
PIN_14_OUT
Text Label 2250 5300 2    50   ~ 0
PIN_14_IN
Text Label 2250 5400 2    50   ~ 0
PIN_15_IN
Text Label 3150 5300 0    50   ~ 0
T_PIN35
Text Label 3150 5400 0    50   ~ 0
T_PIN36
Text Label 2250 5500 2    50   ~ 0
T_PIN37
Text Label 3150 5500 0    50   ~ 0
PIN_14_OUT
Text Label 3150 5600 0    50   ~ 0
PIN_15_OUT
Text Label 8150 1500 0    50   ~ 0
T_PIN38
Text Label 7450 1300 2    50   ~ 0
T_PIN36
$Sheet
S 7700 3200 650  3000
U 5DAB2A9C
F0 "Indicator leds" 50
F1 "leds.sch" 50
F2 "STATUS" I L 7700 3300 50 
F3 "LCD1_TX" I L 7700 3500 50 
F4 "LCD1_RX" I L 7700 3600 50 
F5 "LCD1_CAL" I L 7700 3700 50 
F6 "LCD1_RST" I L 7700 3800 50 
F7 "LCD2_TX" I L 7700 3950 50 
F8 "LCD2_RX" I L 7700 4050 50 
F9 "LCD2_CAL" I L 7700 4150 50 
F10 "LCD2_RST" I L 7700 4250 50 
F11 "LED_CS" I L 7700 4400 50 
F12 "LED_DATA" I L 7700 4500 50 
F13 "LED_CLK" I L 7700 4600 50 
F14 "ADC_CS" I L 7700 4750 50 
F15 "ADC_DATA" I L 7700 4850 50 
F16 "ADC_CLK" I L 7700 4950 50 
F17 "KB_1" I L 7700 5100 50 
F18 "KB_2" I L 7700 5200 50 
F19 "KB_3" I L 7700 5300 50 
F20 "KB_4" I L 7700 5400 50 
F21 "PIN_15" I L 7700 5650 50 
F22 "PIN_14" I L 7700 5550 50 
F23 "KB_JTAG_TDI" I L 7700 5900 50 
F24 "KB_JTAG_TDO" I L 7700 5800 50 
F25 "KB_JTAG_TCK" I L 7700 6100 50 
F26 "KB_JTAG_MS" I L 7700 6000 50 
$EndSheet
Text Label 7600 4250 2    50   ~ 0
LCD2_RST
Text Label 7600 4150 2    50   ~ 0
LCD2_CAL
Text Label 7600 4050 2    50   ~ 0
LCD2_RX
Text Label 7600 3800 2    50   ~ 0
LCD1_RST
Text Label 7600 3700 2    50   ~ 0
LCD1_CAL
Text Label 7600 3600 2    50   ~ 0
LCD1_RX
Wire Wire Line
	7600 3600 7700 3600
Wire Wire Line
	7600 3700 7700 3700
Wire Wire Line
	7600 3800 7700 3800
Wire Wire Line
	7600 4050 7700 4050
Wire Wire Line
	7600 4150 7700 4150
Wire Wire Line
	7600 4250 7700 4250
Text Label 7600 4400 2    50   ~ 0
LED_CS
Text Label 7600 4500 2    50   ~ 0
LED_DATA
Text Label 7600 4600 2    50   ~ 0
LED_CLK
Wire Wire Line
	7700 4400 7600 4400
Wire Wire Line
	7600 4500 7700 4500
Wire Wire Line
	7700 4600 7600 4600
Text Label 7600 5900 2    50   ~ 0
KB_JTAG_TDI
Text Label 7600 6000 2    50   ~ 0
KB_JTAG_MS
Text Label 7600 6100 2    50   ~ 0
KB_JTAG_TCK
Wire Wire Line
	7600 5900 7700 5900
Wire Wire Line
	7600 6000 7700 6000
Wire Wire Line
	7600 6100 7700 6100
Text Label 7600 5650 2    50   ~ 0
T_PIN36
Text Label 7600 5550 2    50   ~ 0
T_PIN35
Text Label 7600 4750 2    50   ~ 0
T_CS0
Text Label 7600 4850 2    50   ~ 0
T_MOSI0
Text Label 7600 4950 2    50   ~ 0
T_SCLK0
Text Label 7600 5800 2    50   ~ 0
KB_JTAG_TDO
Text Label 7600 3950 2    50   ~ 0
T_RX2
Text Label 7600 3500 2    50   ~ 0
T_RX1
Wire Wire Line
	7600 5800 7700 5800
Wire Wire Line
	7600 5650 7700 5650
Wire Wire Line
	7700 5550 7600 5550
Wire Wire Line
	7600 4750 7700 4750
Wire Wire Line
	7600 4850 7700 4850
Wire Wire Line
	7600 4950 7700 4950
Text Label 7600 5100 2    50   ~ 0
T_RX3
Text Label 7600 5200 2    50   ~ 0
T_RX4
Text Label 7600 5300 2    50   ~ 0
T_RX5
Text Label 7600 5400 2    50   ~ 0
T_RX6
Wire Wire Line
	7600 5400 7700 5400
Wire Wire Line
	7600 5300 7700 5300
Wire Wire Line
	7600 5200 7700 5200
Wire Wire Line
	7600 5100 7700 5100
Text Label 7600 3300 2    50   ~ 0
T_PIN5
Wire Wire Line
	7600 3300 7700 3300
Text Label 8150 1700 0    50   ~ 0
T_PIN5
Text Label 8700 1600 0    50   ~ 0
T_PIN30
Text Label 2250 2200 2    50   ~ 0
T_PIN30
Text Label 2250 4050 2    50   ~ 0
T_PIN30
Wire Wire Line
	7600 3500 7700 3500
Wire Wire Line
	7600 3950 7700 3950
Text Label 3200 1300 0    50   ~ 0
T_RX3
Text Label 3200 1400 0    50   ~ 0
T_RX4
Text Label 3200 1500 0    50   ~ 0
T_RX5
Text Label 3200 1600 0    50   ~ 0
T_RX6
Text Label 3350 1800 0    50   ~ 0
T_CS0
Text Label 3350 1900 0    50   ~ 0
T_MOSI0
Text Label 3350 2000 0    50   ~ 0
T_SCLK0
Text Label 2000 5000 0    50   ~ 0
T_CS1
Text Label 2000 5100 0    50   ~ 0
T_MOSI1
Text Label 2000 5200 0    50   ~ 0
T_SCLK1
Wire Wire Line
	2000 3750 2350 3750
Wire Wire Line
	2000 3850 2350 3850
Wire Wire Line
	2000 3550 2350 3550
Wire Wire Line
	2000 3450 2350 3450
Wire Wire Line
	2000 3350 2350 3350
Wire Wire Line
	2000 3150 2350 3150
Text Label 2000 3150 0    50   ~ 0
T_TX1
Text Label 2000 3550 0    50   ~ 0
T_TX2
Text Label 2000 3350 0    50   ~ 0
T_PIN3
Text Label 2000 3450 0    50   ~ 0
T_PIN4
Text Label 2000 3750 0    50   ~ 0
T_PIN24
Text Label 2000 3850 0    50   ~ 0
T_PIN25
Text Label 7450 1200 2    50   ~ 0
T_PIN35
Text Label 8150 1400 0    50   ~ 0
T_PIN37
Text Label 2250 5900 2    50   ~ 0
T_PIN30
Text Label 2250 5600 2    50   ~ 0
T_PIN38
Wire Wire Line
	4700 6750 5450 6750
Wire Wire Line
	4700 7550 5450 7550
$Comp
L archive:barco_encore_teensy3.5 U1
U 1 1 5DAD63C3
P 5350 6950
F 0 "U1" H 5578 6796 50  0000 L CNN
F 1 "teensy3.5" H 5578 6705 50  0000 L CNN
F 2 "encore_teensy:teensy3.5" H 5500 6400 50  0001 C CNN
F 3 "" H 5500 6400 50  0001 C CNN
	1    5350 6950
	1    0    0    -1  
$EndComp
Connection ~ 5450 6750
Wire Wire Line
	5450 6750 6050 6750
Connection ~ 5450 7550
Wire Wire Line
	5450 7550 6050 7550
$Comp
L archive:barco_encore_teensy3.5 U1
U 2 1 5DAE1316
P 1100 3000
F 0 "U1" H 1450 3165 50  0000 C CNN
F 1 "teensy3.5" H 1450 3074 50  0000 C CNN
F 2 "encore_teensy:teensy3.5" H 1250 2450 50  0001 C CNN
F 3 "" H 1250 2450 50  0001 C CNN
	2    1100 3000
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_teensy3.5 U1
U 3 1 5DAE3BBA
P 3650 1050
F 0 "U1" H 4178 796 50  0000 L CNN
F 1 "teensy3.5" H 4178 705 50  0000 L CNN
F 2 "encore_teensy:teensy3.5" H 3800 500 50  0001 C CNN
F 3 "" H 3800 500 50  0001 C CNN
	3    3650 1050
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_teensy3.5 U1
U 4 1 5DAE667B
P 4200 1600
F 0 "U1" H 4869 1421 50  0000 L CNN
F 1 "teensy3.5" H 4869 1330 50  0000 L CNN
F 2 "encore_teensy:teensy3.5" H 4350 1050 50  0001 C CNN
F 3 "" H 4350 1050 50  0001 C CNN
	4    4200 1600
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_teensy3.5 U1
U 5 1 5DAE77A4
P 1800 4800
F 0 "U1" H 2013 4965 50  0000 C CNN
F 1 "teensy3.5" H 2013 4874 50  0000 C CNN
F 2 "encore_teensy:teensy3.5" H 1950 4250 50  0001 C CNN
F 3 "" H 1950 4250 50  0001 C CNN
	5    1800 4800
	-1   0    0    -1  
$EndComp
$Comp
L archive:barco_encore_teensy3.5 U1
U 6 1 5DB15C9D
P 7650 1050
F 0 "U1" H 7800 1215 50  0000 C CNN
F 1 "teensy3.5" H 7800 1124 50  0000 C CNN
F 2 "encore_teensy:teensy3.5" H 7800 500 50  0001 C CNN
F 3 "" H 7800 500 50  0001 C CNN
	6    7650 1050
	1    0    0    -1  
$EndComp
$Comp
L archive:barco_encore_Power_in J2
U 1 1 5DB3DF41
P 4500 7450
F 0 "J2" H 4418 7667 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 4418 7576 50  0000 C CNN
F 2 "Connectors_Phoenix:PhoenixContact_MCV-G_02x5.08mm_Vertical" H 4500 7450 50  0001 C CNN
F 3 "~" H 4500 7450 50  0001 C CNN
	1    4500 7450
	-1   0    0    -1  
$EndComp
$Comp
L archive:Device_R R31
U 1 1 5DB88601
P 4100 3600
F 0 "R31" H 4170 3646 50  0000 L CNN
F 1 "10k" H 4170 3555 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 3600 50  0001 C CNN
F 3 "~" H 4100 3600 50  0001 C CNN
	1    4100 3600
	1    0    0    -1  
$EndComp
$Comp
L archive:Device_R R30
U 1 1 5DB89180
P 4100 3200
F 0 "R30" H 4170 3246 50  0000 L CNN
F 1 "10k" H 4170 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 3200 50  0001 C CNN
F 3 "~" H 4100 3200 50  0001 C CNN
	1    4100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3750 4250 3750
Connection ~ 4100 3750
Wire Wire Line
	4100 3350 4250 3350
Connection ~ 4100 3350
Wire Wire Line
	3050 3750 4100 3750
Wire Wire Line
	3050 3350 4100 3350
Text GLabel 3950 3050 0    50   Input ~ 0
5V
Text GLabel 3950 3450 0    50   Input ~ 0
5V
Wire Wire Line
	3950 3450 4100 3450
Wire Wire Line
	3950 3050 4100 3050
$Comp
L archive:Device_R R32
U 1 1 5DBDDB4D
P 8550 1450
F 0 "R32" H 8620 1496 50  0000 L CNN
F 1 "10k" H 8620 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8480 1450 50  0001 C CNN
F 3 "~" H 8550 1450 50  0001 C CNN
	1    8550 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1600 8550 1600
Wire Wire Line
	8700 1600 8550 1600
Connection ~ 8550 1600
Text GLabel 8450 1250 0    50   Input ~ 0
5V
Wire Wire Line
	8450 1250 8550 1250
Wire Wire Line
	8550 1250 8550 1300
Wire Wire Line
	10050 5000 10150 5000
Wire Wire Line
	10050 5100 10150 5100
Wire Wire Line
	9300 3700 9400 3700
Wire Wire Line
	9400 3900 9300 3900
Wire Wire Line
	9300 4000 9400 4000
Wire Wire Line
	10050 3800 10150 3800
Wire Wire Line
	9300 4400 9400 4400
Wire Wire Line
	10050 4800 10150 4800
$EndSCHEMATC
