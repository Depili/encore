## Reverse engineering Barco Folsom encore controllers

This repository contains materials, pcb's and other information on my quest to reverse engineer Barco's encore SC and LC controllers.

Currently targetting the connection between the "carrier boards" and the control surface. It is a 2x20 1,27mm pitch IDC connector. The signals on the connector are listed in J6.ods file.

The keymap and led-map for the encore SC is now known and shown in sc_kb_map.ods

The kicad directory contains a pcb for interfacing a teensy 3.5 microcontroller with the encore controllers.

Everything else than the manuals is licensed under the MIT license.


### The connector between keyboard and carrier boards

5V logic leves, with V<sub>H</sub> observed as being around 4,6V. The connector has 74FCT16244T line buffers in each end. Their datasheet gives V<sub>L</sub> < 0.8V and V<sub>H</sub> > 2V. Pinout for the connector:

|  Signal    	|  PIN 	|  PIN 	|    Signal    	|
|:---------:	|:--:	|:--:	|:---------:	|
|  LCD_1_RX 	|  1 	|  2 	|    GND    	|
|  LCD_1_TX 	|  3 	|  4 	|    GND    	|
| LCD_1_CAL 	|  5 	|  6 	| LCD_1_RST 	|
|    GND    	|  7 	|  8 	|  LCD_2_RX 	|
|    GND    	|  9 	| 10 	|  LCD_2_TX 	|
|    GND    	| 11 	| 12 	| LCD_2_CAL 	|
| LCD_2_RST 	| 13 	| 14 	|  UNKNOWN  	|
|  UNKNOWN  	| 15 	| 16 	|    GND    	|
|  ADC_CLK  	| 17 	| 18 	|    GND    	|
|  LED_CLK  	| 19 	| 20 	|    GND    	|
|  LED_DATA 	| 21 	| 22 	|    GND    	|
|   LED_CS  	| 23 	| 24 	|    GND    	|
|   ADC_CS  	| 25 	| 26 	|  ADC_DATA 	|
|    GND    	| 27 	| 28 	|  JTAG_TDO 	|
|  JTAG_TDI 	| 29 	| 30 	|  JTAG_TCK 	|
|  JTAG_MS  	| 31 	| 32 	|    GND    	|
|    KB_1   	| 33 	| 34 	|    GND    	|
|    KB_2   	| 35 	| 36 	|    GND    	|
|    KB_3   	| 37 	| 38 	|    GND    	|
|    KB_4   	| 39 	| 40 	|    GND    	|

The function and signal direction of pins 14 and 15 is unknown at this time.

#### Inputs and outputs

The following signals are inputs for the keyboard from the carrier board:
* LCD_1_RX
* LCD_1_CAL
* LCD_1_RST
* LCD_2_RX
* LCD_2_CAL
* LCD_2_RST
* LED_CLK
* LED_DATA
* LED_CS
* JTAG_TDI
* JTAG_TCK
* JTAG_MS

Which leaves outputs:
* LCD_1_TX
* LCD_2_TX
* ADC_CLK
* ADC_DATA
* ADC_CS
* JTAG_TDO
* KB_1
* KB_2
* KB_3
* KB_4

### The led bus
This is a mode 3 (inverted clock phase and polarity) MSB first SPI bus with pin 19 as the CLK, data on pin 21 and CS on pin 23. The packets are 5 bytes long and the format is one byte of address followed by 4 bytes (32 bits) of led bitmap.

Valid addresses for the SC are: 0x00 - 0x09 and 0x30. LC addresses TBA.

### The ADC bus
This is a mode 3 (inverted clock phase and polarity) MSB first SPI bus with pin 17 as CLK, data on pin 26 and CS on pin 25. The bus carries 12 bit ADC measurements from the joystick and T-bar and signals for the encoders (3 on SC, 6 on LC). A message is sent each 300μs. The bus has a clock frequency of 1MHz

The 14 byte message consists of unknown 5 byte "header" followed by t-bar, joystick x, y and z with 12 bits each, 6 bytes total, and ending with 3 bytes containing the encoder pulses. each nibble on the 3 bytes corresponds to a single encoder and 0xF is +1 pulse and 0x1 -1 pulse.

Care should be taken to process every message sent, as encoder pulses are present only on a single message and thus missing messages might cause encoder pulses being lost.

### LCD
The rx & tx lines form a UART with 115200bps, 8 bits per character, no parity, 1 stop bit. They communicate with Amulet display ASCII protocol. Reset is active-high and cal is active low and puts the displays into touch screen calibration mode on reset.

### Keys
The KB_1, KB_2, KB_3 and KB_4 pins are one direction UARTs with 9600bps, 8 bits per character, no parity and 1 stop bit. The MSB of each character sent is 0 for key down events and 1 for key up events. Rest of the bits form the key code. SC uses KB_1 and KB_4, LC uses all.

### JTAG
The JTAG pins form a connection to the EPC2 configuration device for the Altera ACEX FPGA on the keyboard.

## Hardware

* Keys
  * Made by Veetronix
  * Product code example: 146510023-02
  * Seems to be EOL design for the switch body
  * Keycaps seem to be E-series?
    * E0 51 for the large buttons
    * E0 21 for the smaller ones?
* LCDs
  * *Old* Amulet display units
  * Pages stored on display firmware
  * Can change pages and displayed variables via UART
  * Can receive touch events on pre-defined areas on pages
* Joystick
  * 3 axis hall effect industrial joystick
* T-bar
* Main processor
  * IBM Powerpc 405
* Carrier board FPGA
  * Xilinx Spartan II
* Keyboard FPGA
  * Altera ACEX
